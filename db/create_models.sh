# The super mundane stuff

bin/rails g model Q/Item key:string:uniq type:string title:string description:string date:string short_title:string url:string rights:string extra:string --force
bin/rails g model Q/JournalArticle --parent Q::Item --force
bin/rails g model Q/JournalArticleX publication_title:string volume:string issue:string pages:string series:string series_title:string series_text:string journal_abbreviation:string language:string doi:string issn:string archive:string archive_location:string library_catalog:string call_number:string q_item:references --no-timestamps --force
bin/rails g model Q/NewspaperArticle --parent Q::Item --force
bin/rails g model Q/NewspaperArticleX publication_title:string pages:string language:string issn:string archive:string archive_location:string library_catalog:string call_number:string edition:string place:string section:string q_item:references --no-timestamps --force
bin/rails g model Q/Book --parent Q::Item --force
bin/rails g model Q/BookX volume:string series:string language:string archive:string archive_location:string library_catalog:string call_number:string series_number:string number_of_volumes:string edition:string place:string publisher:string num_pages:string isbn:string q_item:references --no-timestamps --force
bin/rails g model Q/BookSection --parent Q::Item --force
bin/rails g model Q/BookSectionX volume:string pages:string series:string language:string archive:string archive_location:string library_catalog:string call_number:string series_number:string number_of_volumes:string edition:string place:string publisher:string isbn:string book_title:string q_item:references --no-timestamps --force
bin/rails g model Q/ConferencePaper --parent Q::Item --force
bin/rails g model Q/ConferencePaperX volume:string pages:string series:string language:string doi:string archive:string archive_location:string library_catalog:string call_number:string place:string publisher:string isbn:string proceedings_title:string conference_name:string q_item:references --no-timestamps --force
bin/rails g model Q/EncyclopediaArticle --parent Q::Item --force
bin/rails g model Q/EncyclopediaArticleX volume:string pages:string series:string language:string archive:string archive_location:string library_catalog:string call_number:string series_number:string number_of_volumes:string edition:string place:string publisher:string isbn:string encyclopedia_title:string q_item:references --no-timestamps --force
bin/rails g model Q/Report --parent Q::Item --force
bin/rails g model Q/ReportX pages:string series_title:string language:string archive:string archive_location:string library_catalog:string call_number:string place:string report_number:string report_type:string institution:string q_item:references --no-timestamps --force
bin/rails g model Q/ComputerProgram --parent Q::Item --force
bin/rails g model Q/ComputerProgramX series_title:string archive:string archive_location:string library_catalog:string call_number:string place:string isbn:string version_number:string system:string company:string programming_language:string q_item:references --no-timestamps --force
bin/rails g model Q/Webpage --parent Q::Item --force
bin/rails g model Q/WebpageX language:string website_title:string website_type:string q_item:references --no-timestamps --force
bin/rails g model Q/BlogPost --parent Q::Item --force
bin/rails g model Q/BlogPostX language:string website_type:string blog_title:string q_item:references --no-timestamps --force
bin/rails g model Q/Thesis --parent Q::Item --force
bin/rails g model Q/ThesisX thesis_type:string university:string place:string num_pages:string language:string archive:string archive_location:string library_catalog:string call_number:string q_item:references --no-timestamps --force

# The interesting stuff

bin/rails g model Q/Source where:string:uniq --force
bin/rails g model Q/TheDate when:datetime year:integer month:integer day:integer --force
bin/rails g model Q/Verb lexeme:string --force
bin/rails g model Q/Happening q_item:references q_verb:references q_source:references --force
bin/rails g model Q/DateHappening q_the_date:references prep:string q_happening:references --force
bin/rails g model Q/Entity name:string first_name:string last_name:string --force
bin/rails g model Q/EntityHappening q_entity:references prep:string q_happening:references --force
bin/rails g model Q/Tag label:string:uniq --force
bin/rails g model Q/TaggedItem q_item:references q_tag:references q_source:references --force
