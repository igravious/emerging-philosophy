CREATE TABLE "schema_migrations" ("version" varchar NOT NULL PRIMARY KEY);
CREATE TABLE "ar_internal_metadata" ("key" varchar NOT NULL PRIMARY KEY, "value" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "entities" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name_or_title" varchar, "part_id" integer, "type_id" integer);
CREATE TABLE "types" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "typename" varchar, "supertype_id" integer);
CREATE TABLE "foos" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "bar" integer, "baz" varchar, "bam" date, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "goos" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "wham" date, "whaz" varchar, "whar" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "records" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "table_name" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "set" varchar);
CREATE UNIQUE INDEX "index_records_on_table_name" ON "records" ("table_name");
CREATE TABLE "pictures" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "title" varchar, "image_type" varchar, "image_id" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_pictures_on_image_type_and_image_id" ON "pictures" ("image_type", "image_id");
CREATE TABLE "q_items" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "key" varchar, "type" varchar, "title" varchar, "description" varchar, "date" varchar, "short_title" varchar, "url" varchar, "rights" varchar, "extra" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_q_items_on_key" ON "q_items" ("key");
CREATE TABLE "q_journal_article_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "publication_title" varchar, "volume" varchar, "issue" varchar, "pages" varchar, "series" varchar, "series_title" varchar, "series_text" varchar, "journal_abbreviation" varchar, "language" varchar, "doi" varchar, "issn" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_ccd858f9d8"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_journal_article_xes_on_q_item_id" ON "q_journal_article_xes" ("q_item_id");
CREATE TABLE "q_newspaper_article_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "publication_title" varchar, "pages" varchar, "language" varchar, "issn" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "edition" varchar, "place" varchar, "section" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_f8a1a97f6c"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_newspaper_article_xes_on_q_item_id" ON "q_newspaper_article_xes" ("q_item_id");
CREATE TABLE "q_book_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "volume" varchar, "series" varchar, "language" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "series_number" varchar, "number_of_volumes" varchar, "edition" varchar, "place" varchar, "publisher" varchar, "num_pages" varchar, "isbn" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_bcb0502017"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_book_xes_on_q_item_id" ON "q_book_xes" ("q_item_id");
CREATE TABLE "q_book_section_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "volume" varchar, "pages" varchar, "series" varchar, "language" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "series_number" varchar, "number_of_volumes" varchar, "edition" varchar, "place" varchar, "publisher" varchar, "isbn" varchar, "book_title" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_422a898027"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_book_section_xes_on_q_item_id" ON "q_book_section_xes" ("q_item_id");
CREATE TABLE "q_conference_paper_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "volume" varchar, "pages" varchar, "series" varchar, "language" varchar, "doi" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "place" varchar, "publisher" varchar, "isbn" varchar, "proceedings_title" varchar, "conference_name" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_11c46455dd"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_conference_paper_xes_on_q_item_id" ON "q_conference_paper_xes" ("q_item_id");
CREATE TABLE "q_encyclopedia_article_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "volume" varchar, "pages" varchar, "series" varchar, "language" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "series_number" varchar, "number_of_volumes" varchar, "edition" varchar, "place" varchar, "publisher" varchar, "isbn" varchar, "encyclopedia_title" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_a364b7f673"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_encyclopedia_article_xes_on_q_item_id" ON "q_encyclopedia_article_xes" ("q_item_id");
CREATE TABLE "q_report_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "pages" varchar, "series_title" varchar, "language" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "place" varchar, "report_number" varchar, "report_type" varchar, "institution" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_1d1a4b3924"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_report_xes_on_q_item_id" ON "q_report_xes" ("q_item_id");
CREATE TABLE "q_computer_program_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "series_title" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "place" varchar, "isbn" varchar, "version_number" varchar, "system" varchar, "company" varchar, "programming_language" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_da32c73330"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_computer_program_xes_on_q_item_id" ON "q_computer_program_xes" ("q_item_id");
CREATE TABLE "q_webpage_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "language" varchar, "website_title" varchar, "website_type" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_4fc7609531"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_webpage_xes_on_q_item_id" ON "q_webpage_xes" ("q_item_id");
CREATE TABLE "q_blog_post_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "language" varchar, "website_type" varchar, "blog_title" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_de2facb74f"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_blog_post_xes_on_q_item_id" ON "q_blog_post_xes" ("q_item_id");
CREATE TABLE "q_the_dates" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "when" datetime, "year" integer NOT NULL, "month" integer, "day" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_q_the_dates_on_year_and_month_and_day" ON "q_the_dates" ("year", "month", "day");
CREATE TABLE "q_verbs" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "lexeme" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_q_verbs_on_lexeme" ON "q_verbs" ("lexeme");
CREATE TABLE "q_happenings" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "q_item_id" integer NOT NULL, "q_verb_id" integer NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_bb2c44dcbe"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
, CONSTRAINT "fk_rails_2d75ab1508"
FOREIGN KEY ("q_verb_id")
  REFERENCES "q_verbs" ("id")
);
CREATE INDEX "index_q_happenings_on_q_item_id" ON "q_happenings" ("q_item_id");
CREATE INDEX "index_q_happenings_on_q_verb_id" ON "q_happenings" ("q_verb_id");
CREATE UNIQUE INDEX "happening_index" ON "q_happenings" ("q_item_id", "q_verb_id");
CREATE TABLE "q_date_happenings" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "q_the_date_id" integer NOT NULL, "prep" varchar NOT NULL, "q_happening_id" integer NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_a91da4ec47"
FOREIGN KEY ("q_the_date_id")
  REFERENCES "q_the_dates" ("id")
, CONSTRAINT "fk_rails_ea362f3ea5"
FOREIGN KEY ("q_happening_id")
  REFERENCES "q_happenings" ("id")
);
CREATE INDEX "index_q_date_happenings_on_q_the_date_id" ON "q_date_happenings" ("q_the_date_id");
CREATE INDEX "index_q_date_happenings_on_q_happening_id" ON "q_date_happenings" ("q_happening_id");
CREATE UNIQUE INDEX "the_date_happening_index" ON "q_date_happenings" ("q_the_date_id", "prep", "q_happening_id");
CREATE TABLE "q_entities" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "first_name" varchar, "last_name" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "q_entity_happenings" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "q_entity_id" integer NOT NULL, "prep" varchar NOT NULL, "q_happening_id" integer NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_4d186b737c"
FOREIGN KEY ("q_entity_id")
  REFERENCES "q_entities" ("id")
, CONSTRAINT "fk_rails_2b6bc8fac0"
FOREIGN KEY ("q_happening_id")
  REFERENCES "q_happenings" ("id")
);
CREATE INDEX "index_q_entity_happenings_on_q_entity_id" ON "q_entity_happenings" ("q_entity_id");
CREATE INDEX "index_q_entity_happenings_on_q_happening_id" ON "q_entity_happenings" ("q_happening_id");
CREATE UNIQUE INDEX "entity_happenings_index" ON "q_entity_happenings" ("q_entity_id", "prep", "q_happening_id");
CREATE TABLE "q_tags" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "label" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_q_tags_on_label" ON "q_tags" ("label");
CREATE TABLE "q_tagged_items" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "q_item_id" integer NOT NULL, "q_tag_id" integer NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_b2beccb146"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
, CONSTRAINT "fk_rails_8632aa0322"
FOREIGN KEY ("q_tag_id")
  REFERENCES "q_tags" ("id")
);
CREATE UNIQUE INDEX "tagged_work_index" ON "q_tagged_items" ("q_item_id", "q_tag_id");
CREATE TABLE "works" ("item" varchar DEFAULT NULL, "itemLabel" varchar DEFAULT NULL, "author" varchar DEFAULT NULL, "authorLabel" varchar DEFAULT NULL, "what" varchar DEFAULT NULL, "whatLabel" varchar DEFAULT NULL, "name" varchar DEFAULT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "rank" float DEFAULT NULL, "underscore" varchar, "populate" boolean DEFAULT 0 NOT NULL);
CREATE TABLE "wikidata" ("q" varchar NOT NULL, "en_label" varchar NOT NULL, "type" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, "label" varchar, "lang" varchar, "link_count" integer, "triple_count" integer, "danker" float DEFAULT 0.0 NOT NULL, "size" integer DEFAULT 0 NOT NULL);
CREATE UNIQUE INDEX "index_wikidata_on_q" ON "wikidata" ("q");
CREATE TABLE "authors_of" ("author_id" varchar NOT NULL, "of_id" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_f01a012605"
FOREIGN KEY ("author_id")
  REFERENCES "wikidata" ("q")
, CONSTRAINT "fk_rails_50552de516"
FOREIGN KEY ("of_id")
  REFERENCES "wikidata" ("q")
);
CREATE UNIQUE INDEX "index_authors_of_on_author_id_and_of_id" ON "authors_of" ("author_id", "of_id");
CREATE TABLE "genres_of" ("genre_id" varchar NOT NULL, "of_id" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_bf669df078"
FOREIGN KEY ("genre_id")
  REFERENCES "wikidata" ("q")
, CONSTRAINT "fk_rails_5542abb163"
FOREIGN KEY ("of_id")
  REFERENCES "wikidata" ("q")
);
CREATE UNIQUE INDEX "index_genres_of_on_genre_id_and_of_id" ON "genres_of" ("genre_id", "of_id");
CREATE TABLE "work_exts" ("wikidata_id" varchar NOT NULL, "underscore" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_f3cf917190"
FOREIGN KEY ("wikidata_id")
  REFERENCES "wikidata" ("q")
);
CREATE UNIQUE INDEX "index_work_exts_on_wikidata_id" ON "work_exts" ("wikidata_id");
CREATE TABLE "schemas" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "wikidata_id" varchar NOT NULL, "wb" varchar, CONSTRAINT "fk_rails_130aa3afc2"
FOREIGN KEY ("wikidata_id")
  REFERENCES "wikidata" ("q")
);
CREATE UNIQUE INDEX "index_schemas_on_wikidata_id" ON "schemas" ("wikidata_id");
CREATE TABLE "whats_of" ("what_id" varchar NOT NULL, "of_id" varchar NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL, CONSTRAINT "fk_rails_92af1c057c"
FOREIGN KEY ("what_id")
  REFERENCES "wikidata" ("q")
, CONSTRAINT "fk_rails_3e902565e3"
FOREIGN KEY ("of_id")
  REFERENCES "wikidata" ("q")
);
CREATE UNIQUE INDEX "index_whats_of_on_what_id_and_of_id" ON "whats_of" ("what_id", "of_id");
CREATE INDEX "index_q_tagged_items_on_q_tag_id" ON "q_tagged_items" ("q_tag_id");
CREATE INDEX "index_q_tagged_items_on_q_item_id" ON "q_tagged_items" ("q_item_id");
CREATE TABLE "q_thesis_xes" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "thesis_type" varchar, "university" varchar, "place" varchar, "num_pages" varchar, "language" varchar, "archive" varchar, "archive_location" varchar, "library_catalog" varchar, "call_number" varchar, "q_item_id" integer, CONSTRAINT "fk_rails_3dd3e140a8"
FOREIGN KEY ("q_item_id")
  REFERENCES "q_items" ("id")
);
CREATE INDEX "index_q_thesis_xes_on_q_item_id" ON "q_thesis_xes" ("q_item_id");
INSERT INTO "schema_migrations" (version) VALUES
('20190324232200'),
('20190324232310'),
('20190402140512'),
('20190404135928'),
('20190406083332'),
('20190416125536'),
('20190503152153'),
('20190503171225'),
('20190503171227'),
('20190503171229'),
('20190503171231'),
('20190503171233'),
('20190503171235'),
('20190503171238'),
('20190503171240'),
('20190503171242'),
('20190503171244'),
('20190503171246'),
('20190503171247'),
('20190503171248'),
('20190503171249'),
('20190503171250'),
('20190503171252'),
('20190503171253'),
('20190503171254'),
('20190503171255'),
('20190504023209'),
('20190509073306'),
('20190509084544'),
('20190509084604'),
('20190515104328'),
('20190519090116'),
('20190519091609'),
('20190519114434'),
('20190519205528'),
('20190519221317'),
('20190520103151'),
('20190520144328'),
('20190520160529'),
('20190523055357'),
('20190531125408'),
('20190531133219'),
('20200724114452'),
('20200724133102');


