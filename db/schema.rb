# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_25_170728) do

  create_table "authors_of", id: false, force: :cascade do |t|
    t.string "author_id", null: false
    t.string "of_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id", "of_id"], name: "index_authors_of_on_author_id_and_of_id", unique: true
  end

  create_table "entities", force: :cascade do |t|
    t.string "name_or_title"
    t.integer "part_id"
    t.integer "type_id"
  end

  create_table "foos", force: :cascade do |t|
    t.integer "bar"
    t.string "baz"
    t.date "bam"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres_of", id: false, force: :cascade do |t|
    t.string "genre_id", null: false
    t.string "of_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["genre_id", "of_id"], name: "index_genres_of_on_genre_id_and_of_id", unique: true
  end

  create_table "goos", force: :cascade do |t|
    t.date "wham"
    t.string "whaz"
    t.integer "whar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.string "title"
    t.string "image_type"
    t.integer "image_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["image_type", "image_id"], name: "index_pictures_on_image_type_and_image_id"
  end

  create_table "q_blog_post_xes", force: :cascade do |t|
    t.string "language"
    t.string "website_type"
    t.string "blog_title"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_blog_post_xes_on_q_item_id"
  end

  create_table "q_book_section_xes", force: :cascade do |t|
    t.string "volume"
    t.string "pages"
    t.string "series"
    t.string "language"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "series_number"
    t.string "number_of_volumes"
    t.string "edition"
    t.string "place"
    t.string "publisher"
    t.string "isbn"
    t.string "book_title"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_book_section_xes_on_q_item_id"
  end

  create_table "q_book_xes", force: :cascade do |t|
    t.string "volume"
    t.string "series"
    t.string "language"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "series_number"
    t.string "number_of_volumes"
    t.string "edition"
    t.string "place"
    t.string "publisher"
    t.string "num_pages"
    t.string "isbn"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_book_xes_on_q_item_id"
  end

  create_table "q_computer_program_xes", force: :cascade do |t|
    t.string "series_title"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "place"
    t.string "isbn"
    t.string "version_number"
    t.string "system"
    t.string "company"
    t.string "programming_language"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_computer_program_xes_on_q_item_id"
  end

  create_table "q_conference_paper_xes", force: :cascade do |t|
    t.string "volume"
    t.string "pages"
    t.string "series"
    t.string "language"
    t.string "doi"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "place"
    t.string "publisher"
    t.string "isbn"
    t.string "proceedings_title"
    t.string "conference_name"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_conference_paper_xes_on_q_item_id"
  end

  create_table "q_date_happenings", force: :cascade do |t|
    t.integer "q_the_date_id", null: false
    t.string "prep", null: false
    t.integer "q_happening_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["q_happening_id"], name: "index_q_date_happenings_on_q_happening_id"
    t.index ["q_the_date_id", "prep", "q_happening_id"], name: "the_date_happening_index", unique: true
    t.index ["q_the_date_id"], name: "index_q_date_happenings_on_q_the_date_id"
  end

  create_table "q_encyclopedia_article_xes", force: :cascade do |t|
    t.string "volume"
    t.string "pages"
    t.string "series"
    t.string "language"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "series_number"
    t.string "number_of_volumes"
    t.string "edition"
    t.string "place"
    t.string "publisher"
    t.string "isbn"
    t.string "encyclopedia_title"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_encyclopedia_article_xes_on_q_item_id"
  end

  create_table "q_entities", force: :cascade do |t|
    t.string "name"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "q_entity_happenings", force: :cascade do |t|
    t.integer "q_entity_id", null: false
    t.string "prep", null: false
    t.integer "q_happening_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["q_entity_id", "prep", "q_happening_id"], name: "entity_happenings_index", unique: true
    t.index ["q_entity_id"], name: "index_q_entity_happenings_on_q_entity_id"
    t.index ["q_happening_id"], name: "index_q_entity_happenings_on_q_happening_id"
  end

  create_table "q_happenings", force: :cascade do |t|
    t.integer "q_item_id", null: false
    t.integer "q_verb_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "q_source_id"
    t.index ["q_source_id"], name: "index_q_happenings_on_q_source_id"
  end

  create_table "q_items", force: :cascade do |t|
    t.string "key"
    t.string "type"
    t.string "title"
    t.string "description"
    t.string "date"
    t.string "short_title"
    t.string "url"
    t.string "rights"
    t.string "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_q_items_on_key", unique: true
  end

  create_table "q_journal_article_xes", force: :cascade do |t|
    t.string "publication_title"
    t.string "volume"
    t.string "issue"
    t.string "pages"
    t.string "series"
    t.string "series_title"
    t.string "series_text"
    t.string "journal_abbreviation"
    t.string "language"
    t.string "doi"
    t.string "issn"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_journal_article_xes_on_q_item_id"
  end

  create_table "q_newspaper_article_xes", force: :cascade do |t|
    t.string "publication_title"
    t.string "pages"
    t.string "language"
    t.string "issn"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "edition"
    t.string "place"
    t.string "section"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_newspaper_article_xes_on_q_item_id"
  end

  create_table "q_report_xes", force: :cascade do |t|
    t.string "pages"
    t.string "series_title"
    t.string "language"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.string "place"
    t.string "report_number"
    t.string "report_type"
    t.string "institution"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_report_xes_on_q_item_id"
  end

  create_table "q_sources", force: :cascade do |t|
    t.string "where", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["where"], name: "index_q_sources_on_where", unique: true
  end

  create_table "q_tagged_items", force: :cascade do |t|
    t.integer "q_item_id", null: false
    t.integer "q_tag_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "q_source_id"
    t.index ["q_source_id"], name: "index_q_tagged_items_on_q_source_id"
  end

  create_table "q_tags", force: :cascade do |t|
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["label"], name: "index_q_tags_on_label", unique: true
  end

  create_table "q_the_dates", force: :cascade do |t|
    t.datetime "when"
    t.integer "year", null: false
    t.integer "month"
    t.integer "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year", "month", "day"], name: "index_q_the_dates_on_year_and_month_and_day", unique: true
  end

  create_table "q_thesis_xes", force: :cascade do |t|
    t.string "thesis_type"
    t.string "university"
    t.string "place"
    t.string "num_pages"
    t.string "language"
    t.string "archive"
    t.string "archive_location"
    t.string "library_catalog"
    t.string "call_number"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_thesis_xes_on_q_item_id"
  end

  create_table "q_verbs", force: :cascade do |t|
    t.string "lexeme", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lexeme"], name: "index_q_verbs_on_lexeme", unique: true
  end

  create_table "q_webpage_xes", force: :cascade do |t|
    t.string "language"
    t.string "website_title"
    t.string "website_type"
    t.integer "q_item_id"
    t.index ["q_item_id"], name: "index_q_webpage_xes_on_q_item_id"
  end

  create_table "records", force: :cascade do |t|
    t.string "table_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "set"
    t.index ["table_name"], name: "index_records_on_table_name", unique: true
  end

  create_table "schemas", force: :cascade do |t|
    t.string "wikidata_id", null: false
    t.string "wb"
    t.index ["wikidata_id"], name: "index_schemas_on_wikidata_id", unique: true
  end

  create_table "types", force: :cascade do |t|
    t.string "typename"
    t.integer "supertype_id"
  end

  create_table "whats_of", id: false, force: :cascade do |t|
    t.string "what_id", null: false
    t.string "of_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["what_id", "of_id"], name: "index_whats_of_on_what_id_and_of_id", unique: true
  end

  create_table "wikidata", id: false, force: :cascade do |t|
    t.string "q", null: false
    t.string "en_label", null: false
    t.string "type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "label"
    t.string "lang"
    t.integer "link_count"
    t.integer "triple_count"
    t.float "danker", default: 0.0, null: false
    t.integer "size", default: 0, null: false
    t.index ["q"], name: "index_wikidata_on_q", unique: true
  end

  create_table "work_exts", id: false, force: :cascade do |t|
    t.string "wikidata_id", null: false
    t.string "underscore"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["wikidata_id"], name: "index_work_exts_on_wikidata_id", unique: true
  end

  create_table "works", id: false, force: :cascade do |t|
    t.string "item"
    t.string "itemLabel"
    t.string "author"
    t.string "authorLabel"
    t.string "what"
    t.string "whatLabel"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "rank"
    t.string "underscore"
    t.boolean "populate", default: false, null: false
  end

end
