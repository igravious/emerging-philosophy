class CreateQTags < ActiveRecord::Migration[5.2]
  def change
    create_table :q_tags do |t|
      t.string :label, :null => false
      t.index :label, :unique => true

      t.timestamps
    end
  end
end
