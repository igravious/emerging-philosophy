# Don't know whether to call them Parts, Entities, or Individuals :/

class CreateEntities < ActiveRecord::Migration[5.2]
  def change
    create_table :entities do |t|
			t.string :name_or_title	# entities have names or titles because they're 
															# things like people, institutions, major events, works of art, …
      t.integer :part_id			# allows for composition (part-of)
      t.integer :type_id			# everthing has a type (which has its own hierarchy)

      # t.timestamps
			# use paper_trail instead
			# there'll also be a table where attributes and values are tracked, and versioned :/
    end
  end
end
