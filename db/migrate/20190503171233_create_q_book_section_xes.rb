class CreateQBookSectionXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_book_section_xes do |t|
      t.string :volume
      t.string :pages
      t.string :series
      t.string :language
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.string :series_number
      t.string :number_of_volumes
      t.string :edition
      t.string :place
      t.string :publisher
      t.string :isbn
      t.string :book_title
      t.references :q_item, foreign_key: true
    end
  end
end
