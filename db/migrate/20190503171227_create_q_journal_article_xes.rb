class CreateQJournalArticleXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_journal_article_xes do |t|
      t.string :publication_title
      t.string :volume
      t.string :issue
      t.string :pages
      t.string :series
      t.string :series_title
      t.string :series_text
      t.string :journal_abbreviation
      t.string :language
      t.string :doi
      t.string :issn
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.references :q_item, foreign_key: true
    end
  end
end
