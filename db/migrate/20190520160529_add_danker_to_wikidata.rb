class AddDankerToWikidata < ActiveRecord::Migration[5.2]
  def change
    add_column :wikidata, :danker, :float, default: 0.0, null: false
  end
end
