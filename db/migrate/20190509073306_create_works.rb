class CreateWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :works, id: false do |t|
      t.string :item
      t.string :itemLabel
      t.string :author
      t.string :authorLabel
      t.string :what
      t.string :whatLabel
      t.string :name

      t.timestamps
    end
  end
end
