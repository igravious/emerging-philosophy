class CreateQThesisXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_thesis_xes do |t|
      t.string :thesis_type
      t.string :university
      t.string :place
      t.string :num_pages
      t.string :language
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.references :q_item, foreign_key: true
    end
  end
end
