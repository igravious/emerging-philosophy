class AddUnderscoreToWork < ActiveRecord::Migration[5.2]
  def change
    add_column :works, :underscore, :string
  end
end
