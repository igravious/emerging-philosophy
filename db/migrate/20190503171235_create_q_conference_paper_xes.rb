class CreateQConferencePaperXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_conference_paper_xes do |t|
      t.string :volume
      t.string :pages
      t.string :series
      t.string :language
      t.string :doi
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.string :place
      t.string :publisher
      t.string :isbn
      t.string :proceedings_title
      t.string :conference_name
      t.references :q_item, foreign_key: true
    end
  end
end
