class AddRankToWork < ActiveRecord::Migration[5.2]
  def change
    add_column :works, :rank, :float
  end
end
