class CreateQHappenings < ActiveRecord::Migration[5.2]
  def change
    create_table :q_happenings do |t|
      t.references :q_item, foreign_key: true, :null => false
      t.references :q_verb, foreign_key: true, :null => false
			t.index [:q_item_id, :q_verb_id], :unique => true, :name => 'happening_index'

      t.timestamps
    end
  end
end
