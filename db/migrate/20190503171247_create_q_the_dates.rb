class CreateQTheDates < ActiveRecord::Migration[5.2]
  def change
    create_table :q_the_dates do |t|
      t.datetime :when
      t.integer :year, :null => false
      t.integer :month
      t.integer :day
			t.index [:year, :month, :day], :unique => true

      t.timestamps
    end
  end
end
