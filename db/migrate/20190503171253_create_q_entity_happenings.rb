class CreateQEntityHappenings < ActiveRecord::Migration[5.2]
  def change
    create_table :q_entity_happenings do |t|
      t.references :q_entity, foreign_key: true, :null => false
      t.string :prep, :null => false
      t.references :q_happening, foreign_key: true, :null => false
      t.index [:q_entity_id, :prep, :q_happening_id], :unique => true, :name => 'entity_happenings_index'

      t.timestamps
    end
  end
end
