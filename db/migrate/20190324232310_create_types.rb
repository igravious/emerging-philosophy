class CreateTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :types do |t|
      t.string :typename			# every type is called something
      t.integer :supertype_id	# types are composable (subtyping/supertyping)
															# they're composable in a different way to how entities are

      # t.timestamps
			# using papertrail eventually
    end
  end
end
