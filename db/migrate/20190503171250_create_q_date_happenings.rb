class CreateQDateHappenings < ActiveRecord::Migration[5.2]
  def change
    create_table :q_date_happenings do |t|
      t.references :q_the_date, foreign_key: true, :null => false
      t.string :prep, :null => false
      t.references :q_happening, foreign_key: true, :null => false
      t.index [:q_the_date_id, :prep, :q_happening_id], :unique => true, :name => 'the_date_happening_index'

      t.timestamps
    end
  end
end
