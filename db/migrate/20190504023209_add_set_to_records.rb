class AddSetToRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :set, :string
  end
end
