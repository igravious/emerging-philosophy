class CreateWikidata < ActiveRecord::Migration[5.2]
  def change
    create_table :wikidata, id: false do |t|
      t.string :q, null: false
      t.string :en_label, null: false
      t.string :type, null: false

      t.timestamps
			# ?
			# SQLite3::SQLException: foreign key mismatch - "instances_of" referencing "wikidata": INSERT INTO "instances_of" ("instance_id", "of_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)
			#
    	# t.index [:q, :type], unique: true
    	t.index :q, unique: true
    end
  end
end
