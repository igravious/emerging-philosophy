class AddSourceRefToQTaggedItems < ActiveRecord::Migration[5.2]
  def change
    add_reference :q_tagged_items, :q_source, foreign_key: true
  end
end
