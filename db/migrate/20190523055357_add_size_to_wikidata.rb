class AddSizeToWikidata < ActiveRecord::Migration[5.2]
  def change
    add_column :wikidata, :size, :integer, default: 0, null: false
  end
end
