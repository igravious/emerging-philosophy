class AddUsefulFieldsToWikidata < ActiveRecord::Migration[5.2]
  def change
    add_column :wikidata, :label, :string
    add_column :wikidata, :lang, :string
    add_column :wikidata, :link_count, :integer
    add_column :wikidata, :triple_count, :integer
  end
end
