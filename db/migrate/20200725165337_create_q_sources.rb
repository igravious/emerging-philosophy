class CreateQSources < ActiveRecord::Migration[5.2]
  def change
    create_table :q_sources do |t|
      t.string :where, :null => false
      t.index :where, :unique => true

      t.timestamps
    end
  end
end
