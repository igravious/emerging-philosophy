class CreateQEntities < ActiveRecord::Migration[5.2]
  def change
    create_table :q_entities do |t|
      t.string :name
      t.string :first_name
      t.string :last_name

      t.timestamps
    end
  end
end
