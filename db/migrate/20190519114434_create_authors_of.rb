class CreateAuthorsOf < ActiveRecord::Migration[5.2]
  def change
    create_table :authors_of, id: false do |t|
      t.references :author, type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false
      t.references :of,     type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false

      t.timestamps
      t.index [:author_id, :of_id], :unique => true
    end
  end
end
