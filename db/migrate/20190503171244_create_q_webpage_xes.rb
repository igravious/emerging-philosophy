class CreateQWebpageXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_webpage_xes do |t|
      t.string :language
      t.string :website_title
      t.string :website_type
      t.references :q_item, foreign_key: true
    end
  end
end
