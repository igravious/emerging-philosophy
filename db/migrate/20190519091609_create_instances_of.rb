class CreateInstancesOf < ActiveRecord::Migration[5.2]
  def up
    create_table :instances_of, id: false do |t|
      t.references :instance, type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false
      t.references :of,       type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false

      t.timestamps
			# t.foreign_key :wikidata, column: :instance_id, primary_key: :q
			# t.foreign_key :wikidata, column: :of_id, primary_key: :q
      t.index [:instance_id, :of_id], :unique => true
    end
		# execute "ALTER TABLE instances_of ADD CONSTRAINT pk_instances_of PRIMARY KEY (instance_id,of_id);"
  end

	def down
		drop_table :instances_of
	end
end
