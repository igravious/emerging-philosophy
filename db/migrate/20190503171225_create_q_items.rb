class CreateQItems < ActiveRecord::Migration[5.2]
  def change
    create_table :q_items do |t|
      t.string :key, :null => false    # can't not have a key
      t.string :type, :null => false   # can't not have a type
      t.string :title, :null => false  # can't not have a title
      t.string :date, :null => false   # must at least be narrowed down to which year
      t.string :description # doesn't have to have one, but really should
      t.string :url         # same
      t.string :rights      # same
      t.string :short_title
      t.string :extra

      t.timestamps
    end
    add_index :q_items, :key, unique: true
  end
end
