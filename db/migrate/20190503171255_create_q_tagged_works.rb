class CreateQTaggedWorks < ActiveRecord::Migration[5.2]
  def change
    create_table :q_tagged_works do |t|
      t.references :q_item, foreign_key: true, :null => false
      t.references :q_tag, foreign_key: true, :null => false
      t.index [:q_item_id, :q_tag_id], :unique => true, :name => 'tagged_work_index'

      t.timestamps
    end
  end
end
