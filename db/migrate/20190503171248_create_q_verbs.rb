class CreateQVerbs < ActiveRecord::Migration[5.2]
  def change
    create_table :q_verbs do |t|
      t.string :lexeme, :null => false
      t.index :lexeme, :unique => true

      t.timestamps
    end
  end
end
