class RenameTableQTaggedWorksToQTaggedItems < ActiveRecord::Migration[5.2]
  def change
		rename_table :q_tagged_works, :q_tagged_items
  end
end
