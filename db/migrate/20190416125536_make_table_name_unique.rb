class MakeTableNameUnique < ActiveRecord::Migration[5.2]
  def change
		add_index :records, :table_name, unique: true
  end
end
