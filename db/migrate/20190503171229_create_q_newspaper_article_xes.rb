class CreateQNewspaperArticleXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_newspaper_article_xes do |t|
      t.string :publication_title
      t.string :pages
      t.string :language
      t.string :issn
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.string :edition
      t.string :place
      t.string :section
      t.references :q_item, foreign_key: true
    end
  end
end
