class CreateQReportXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_report_xes do |t|
      t.string :pages
      t.string :series_title
      t.string :language
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.string :place
      t.string :report_number
      t.string :report_type
      t.string :institution
      t.references :q_item, foreign_key: true
    end
  end
end
