class AddSourceRefToQHappenings < ActiveRecord::Migration[5.2]
  def change
    add_reference :q_happenings, :q_source, foreign_key: true
  end
end
