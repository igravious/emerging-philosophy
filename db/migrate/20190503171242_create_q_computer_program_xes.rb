class CreateQComputerProgramXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_computer_program_xes do |t|
      t.string :series_title
      t.string :archive
      t.string :archive_location
      t.string :library_catalog
      t.string :call_number
      t.string :place
      t.string :isbn
      t.string :version_number
      t.string :system
      t.string :company
      t.string :programming_language
      t.references :q_item, foreign_key: true
    end
  end
end
