class CreateQBlogPostXes < ActiveRecord::Migration[5.2]
  def change
    create_table :q_blog_post_xes do |t|
      t.string :language
      t.string :website_type
      t.string :blog_title
      t.references :q_item, foreign_key: true
    end
  end
end
