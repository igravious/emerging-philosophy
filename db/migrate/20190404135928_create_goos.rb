class CreateGoos < ActiveRecord::Migration[5.2]
  def change
    create_table :goos do |t|
      t.date :wham
      t.string :whaz
      t.integer :whar

      t.timestamps
    end
  end
end
