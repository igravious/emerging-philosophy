class CreateWorkExts < ActiveRecord::Migration[5.2]
  def change
    create_table :work_exts, id: false do |t|
      t.references :wikidata, type: :string, foreign_key: {primary_key: :q}, null: false, index: {unique: true}
      t.string :underscore

      t.timestamps
      # t.index :wikidata_id, :unique => true
    end
  end
end
