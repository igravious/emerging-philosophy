class DropInstancesOf < ActiveRecord::Migration[5.2]
  def change
    drop_table :instances_of, id: false do |t|
      t.references :instance, type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false
      t.references :of,       type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false

      t.timestamps
      t.index [:instance_id, :of_id], :unique => true
	  end
  end
end
