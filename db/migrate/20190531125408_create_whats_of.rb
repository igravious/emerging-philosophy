class CreateWhatsOf < ActiveRecord::Migration[5.2]
  def up
    create_table :whats_of, id: false do |t|
      t.references :what, type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false
      t.references :of,   type: :string, foreign_key: {to_table: :wikidata, primary_key: :q}, null: false, index: false

      t.timestamps
      t.index [:what_id, :of_id], :unique => true
    end
  end

	def down
		# what about dropping indexes? (uh, indices?)
		drop_table :whats_of
	end
end
