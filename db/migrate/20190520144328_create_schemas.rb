class CreateSchemas < ActiveRecord::Migration[5.2]
  def change
    create_table :schemas do |t|
      t.references :wikidata, type: :string, foreign_key: {primary_key: :q}, null: false, index: {unique: true}
			t.string :wb
    end
  end
end
