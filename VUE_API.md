
first off

take care of:
    config.api_only = true
in config/application.rb

bin/rails g scaffold api/blah name:string

### URLs

DIY

https://sourcey.com/articles/building-the-perfect-rails-api
https://web-crunch.com/ruby-on-rails-api-vue-js/

GEMS

https://github.com/tuwukee/jwt_sessions
https://github.com/rails-api/active_model_serializers

TABLES

http://tabulator.info/docs/4.1/frameworks#vue
https://www.npmjs.com/package/vue-tabulator
https://github.com/angeliski/vue-tabulator#readme
https://www.npmtrends.com/datatables.net-vs-datatables-vs-ag-grid-vs-ag-grid-enterprise
https://editor.datatables.net/
https://madewithvuejs.com/blog/best-vue-js-datatables
https://www.ag-grid.com/vue-getting-started/

