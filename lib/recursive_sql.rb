
# indebted to:
#   https://hashrocket.com/blog/posts/recursive-sql-in-activerecord
#   https://github.com/take-five/activerecord-hierarchical_query
#   https://en.wikipedia.org/wiki/Hierarchical_and_recursive_queries_in_SQL
#   https://www.sqlite.org/lang_with.html
#   https://sqlite.org/releaselog/3_8_3.html

module RecursiveSQL

	module InstanceMethods
		
		def level_representation
			# indexed first by column name of result set, then by integer
			[{"level"=>0,"id"=>self.id ,0=>0,1=>self.id}]
		end

		def id_representation
			# indexed first by column name of result set, then by integer
			[{"id"=>self.id ,0=>self.id}]
		end

		def tree_with_attrs
			all_with_attrs - [self]
		end

		def tree_with_ids
			all_with_ids - self.id_representation
		end

		def tree_with_levels
			all_with_levels - self.level_representation
		end

		def chain
			self.becomes(self.class).class
		end

		def all_with_attrs
			self.chain.attrs_for(self)
		end

		def all_with_ids
			self.chain.ids_for(self)
		end

		def all_with_levels
			self.chain.levels_for(self)
		end

	end

	module ClassMethods

		def attrs_for(this, desc=true) # never want attributes
			# activerecord resultset
			where("#{table_name}.id IN (#{sqlite_with_recursive(this, desc, false)})")
		end

		def ids_for(this, desc=true) # we could verify attributes?
			# execute raw SELECT giving an array of ids
			ActiveRecord::Base.connection.execute(sqlite_with_recursive(this, desc, false))
		end

		def levels_for(this, desc=true) # we could verify attributes?
			# execute raw SELECT giving an array of levels and ids
			ActiveRecord::Base.connection.execute(sqlite_with_recursive(this, desc, true))
		end

		# need to make this work for postgres and mysql and oracle and …

		# https://www.sqlite.org/lang_with.html
		# you can plot a mandelbrot fractal with this = mind blown
		def sqlite_with_recursive(this, desc, include_level)
			# byebug
			throw "id for record is NULL, that's rarely a good sign" if this.id.nil?
			# VALUES(0,…) sets level to zero, and id to this record's id
			tree_sql = <<~SQL
				WITH RECURSIVE
					under_node(level,id) AS (
						VALUES(0,#{this.id})
						UNION ALL
						SELECT under_node.level+1, #{table_name}.id
							FROM #{table_name} JOIN under_node ON #{table_name}.#{link_name}=under_node.id
							ORDER BY 1 #{desc ? 'DESC' : ''}
					)
				SELECT #{include_level ? '*' : 'id'} from under_node
			SQL
		end

	end

end
