
# https://api.rubyonrails.org/classes/ActiveSupport/Inflector.html
#
# path style table name -> moduled model string
# irb(main):010:0> "prefix/wows".singularize.classify
# => "Prefix::Wow"
#
# opposite
#
# moduled model string (Foo.to_s) -> path style table name
# irb(main):011:0> "Prefix::Wow".underscore.pluralize
# => "prefix/wows"

def __prefix
	Rails.configuration.x.zotero.prefix.underscore+'_'
end

def __path
	Rails.configuration.x.zotero.prefix.underscore+'/'
end

#
# q_foos -> Q::Foo
# q_snake_foos -> Q::SnakeFoo
# snake_foos -> SnakeFoo
#
def tbl2mdl(tbl)
	__class(tbl.singularize)
end
#
# Q::Foo -> q_foos
# Q::SnakeFoo -> q_snake_foos
# SnakeFoo -> snake_foos
#
def mdl2tbl(mdl)
	__str(mdl).pluralize
end

def mdl2row(mdl)
	__str(mdl)
end

# make fully qualified model name: FQMN
def prefixed_const(str)
	prefix = Rails.configuration.x.zotero.prefix
	Object.const_get(prefix+'::'+str) # .constantize
end

def prefixed_consts
	return [
		prefixed_const('Verb'),
		prefixed_const('Happening'),
		prefixed_const('Entity'),
		prefixed_const('EntityHappening'),
		prefixed_const('TheDate'),
		prefixed_const('DateHappening'),
		prefixed_const('Tag'),
		prefixed_const('TaggedItem'),
		prefixed_const('Source')
	]
end

def type_name_ext(type)
	suffix = Rails.configuration.x.zotero.suffix
	(type.to_s+suffix).underscore
end

#
# Model -> q_model (:references takes a singular table name, confusingly)
# CamelCased unprefixed model string -> singular table name
def prefixed_ref(str) # is this right?
	"#{mdl2row(prefixed_const(str))}:references"
end

def __class(path)
	path.sub(/^#{__prefix}/,"#{__path}").classify.constantize
end

def __str(model)
	model.to_s.underscore.sub('/','_')
end

def __sym(model)
	__str(model).to_sym
end

