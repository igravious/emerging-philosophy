
Dir[File.join(".", "**/canon*.rb")].each do |f|
	require f
end

# config.x.canon.wikidata = 'https://query.wikidata.org/sparql'

namespace :canon do

	def one_by_one(klass, query_str, in_order={})
		const = Object.const_get(klass)
		things = const.order(in_order)
		exit if things.nil?
		total = things.length
		bar = progress_bar(total, true)
		require 'sparql/client'
		wikidata = Rails.configuration.x.canon.wikidata
		w = SPARQL::Client.new(wikidata)
		things.each {|thing|
			w_str = query_str % {:interpolated_entity => thing.q}
			solution_set = w.query(w_str)
			yield solution_set, thing
			update_progress(bar)
		}
	end

  def progress_bar(total, force=false, objects='records')
    @progress = 0
    @total = total
    bar = nil
    if not system("[ -t 1 ]") or force
      require 'progress_bar'
      bar = ProgressBar.new(total, :bar, :counter, :percentage, :elapsed, :eta)
    end
    str = "About to process #{@total} #{objects}"
    if bar.nil?
      puts str
    else
      STDERR.puts str
    end
    bar
  end

	def update_progress(bar, msg='')
    if not bar.nil?
      bar.increment!
    else
      @progress += 1
			if msg.empty?
      	STDERR.puts "Processing #{@progress} of #{@total}"
			else
      	STDERR.puts "#{msg} – processing #{@progress} of #{@total}"
			end
    end
  end
end
