
Dir[File.join(".", "**/zotero*.rb")].each do |f|
	require f
end

#                config.x.zotero.raw_data_dir = 'tmp/raw_data/'
#                config.x.zotero.key_file = 'config/zotero.key'
#                config.x.zotero.item_type = :item
#                config.x.zotero.prefix = 'Q' # surprise, surprise :/
#                config.x.zotero.suffix = 'X'
#                config.x.zotero.collection = 'emerging philosophy'
#                config.x.zotero.id = 1248965
#                config.x.zotero.the_works = '_zotero_works.txt'
#                config.x.zotero.the_attachments = '_zotero_attachments.txt'
#                config.x.zotero.the_entries = '_zotero_entries.txt'

namespace :zotero do
	# this function is used by: show_parse, load, namespace
	def shared_parse
		dir = Rails.configuration.x.zotero.raw_data_dir
		it = Rails.configuration.x.zotero.item_type
		fn_works = Rails.root.join(dir, '*'+Rails.configuration.x.zotero.the_works)
		str = File.read(Dir[fn_works].last)
		len = str.lines.length
		# puts "#{len} entries"
		freq_attrs = {}
		item_types = []
		attrs_in = {}
		tables = {}
		tables[it] = []
		works = []
		str.lines do |work|
			rec = JSON.parse(work)
			rec.keys.each do |attr|
				if freq_attrs.key?(attr)
					freq_attrs[attr] += 1
				else
					freq_attrs[attr] = 1
				end
			end
			works.push(rec)
		end
		works.each do |work|
			# https://www.zotero.org/support/kb/item_types_and_fields
			type = work['itemType'].to_sym
			if tables.key?(type) # never changing
				# done already
			else
				tables[type] = []
				work.keys.each do |attr|
					if len == freq_attrs[attr]
						tables[it].push(attr) unless attr == 'itemType' or tables[it].include?(attr)
					else
						tables[type].push(attr)
					end
				end
			end
			work.keys.each do |attr|
				if len == freq_attrs[attr]
					item_types.push(type) unless item_types.include?(type)
				else
					if attrs_in.key?(attr)
						attrs_in[attr].push(type) unless attrs_in[attr].include?(type)
					else
						attrs_in[attr] = [type]
					end
				end
			end
		end
		attrs_in.each do |attr, types|
			attrs_in[attr] = types.uniq
		end
		# puts attrs_in
		freq_attrs.each do |attr, value|
			if len == value
				# puts "### #{attr} (#{works.first[attr].class})"
			else
				# puts "#{attr} (String) => #{value} #{attrs_in[attr]}"
			end
		end
		# puts freq_attrs
		# puts item_types
		[freq_attrs, item_types, attrs_in, tables, works]
	end
end
