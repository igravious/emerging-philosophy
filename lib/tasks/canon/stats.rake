namespace :canon do

	desc "show some Work stats"
	task stats: :environment do
		fn = Rails.root.join('db','philosophers.json')
		phils = JSON.parse(File.read(fn))
		# puts phils.length
		#works = Work.where.not(rank: nil).order(rank: :desc).pluck(:name, :authorLabel, :author)
		works = Work.where.not(size: 0).order(size: :desc)

		excluded = [
			7012  # ranked dead last (exclude)
		]
		suspect = [
			1379, # Joseph ben Ephraim Karo
			682,  # Patanjali
		]
		literature = [
			436,  # Geoffrey Chaucer
			243,  # Lewis Carroll
			85,   # Dante Alighieri
			332,  # Leo Tolstoy
			557,  # Jonathan Swift
			288,  # C. S. Lewis
			279   # Fyodor Dostoyevsky
		]
		# require 'pry'
		# binding.pry
		works.each_with_index do |work, i|
			p work
			match = false
			count = 1
			phils.each do |phil|
				p phil
				a = 'Q'+phil['entity_id'].to_s
				p a
				authors = work.authors
				p authors
				exit
				b = work[2]
				# puts "#{a.inspect} == #{b.inspect}"
				if a == b
					match = true
					pos = phil['measure_pos']
					if count > 9999
						STDERR.puts "!!!"
					end
					if pos != count
						STDERR.printf("[%04d][%05d] %02d: “%s” by %s\n", pos, count, i+1, work[0], work[1])
					end
					if excluded.include?(pos)
						printf("[----][%05d] %02d: “%s” by %s\n", count, i+1, work[0], work[1])
					elsif literature.include?(count)
						printf("[====][%05d] %02d: “%s” by %s\n", count, i+1, work[0], work[1])
					else
						printf("[%04d][%05d] %02d: “%s” by %s\n", pos, count, i+1, work[0], work[1])
					end
					break
				end
				count += 1
			end
			if not match
				printf("[    ][     ] %02d: “%s” by %s\n", i+1, work[0], work[1])
			end
		end
	end

end
