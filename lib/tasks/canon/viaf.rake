namespace :canon do

	namespace :viaf do

		def viaf_xml(id)
			"https://viaf.org/viaf/#{id}/viaf.xml"
		end

		desc "populate Work (works table) using philosopher Viaf data"
		task philosophers: :environment do

			fn = Rails.root.join('db','philosophers.json')
			phils = JSON.parse(File.read(fn))
			# {"entity_id"=>859, "viaf"=>"108159964", "measure_pos"=>1, "lang"=>"en", "label"=>"Plato"}

			require 'net/http'
			phils.each {|phil|
				str = viaf_id(phil['viaf'].to_i)
				puts "[#{phil['label']}]"
				phil_q = 'Q'+phil['entity_id'].to_s
				work_qs = []
				parse_viaf(str) do |work_q|
					work_qs.push(work_q)
				end
			}
		end

		desc "test viaf url with plato xml url"
		task plato_url: :environment do
			require 'net/http'
			puts viaf_id(108159964)
		end

		def viaf_id(id)
			# require 'oclc/auth'
			# wskey = OCLC::Auth::WSKey.new('api-key', 'api-key-secret')

			url = viaf_xml(id)
			uri = URI.parse(url)

			request = Net::HTTP::Get.new(uri.request_uri)
			# request['Authorization'] = wskey.hmac_signature('GET', url, :principal_id => 'principal-ID', :principal_idns => 'principal-IDNS')

			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			response = http.start do |http| 
				http.request(request)
			end
			response.body
		end

		def parse_viaf(obj)
			doc = Nokogiri::XML(obj)
			# require 'open-uri'
			# doc = Nokogiri::HTML(open(viaf_xml(108159964)))
			wikidata = Rails.configuration.x.canon.wikidata
			w = SPARQL::Client.new(wikidata)
			doc.xpath('//ns1:work').each {|w|
				viaf_id = w["id"]
				node_set = w.xpath('ns1:title')
				title = node_set.first.content
				if viaf_id.nil?
				elsif viaf_id.empty?
				else
					parts = viaf_id.split('|')
					if 2 == parts.length
						if 'VIAF' == parts.first
							q = "SELECT ?item WHERE { ?item wdt:P214 '#{parts.second}' }"
							solutions = w.query(q)
							len = solutions.length
							if 0 == len
								# puts "#{viaf_id} =>"
							elsif 1 == len
								solution = solutions.first
								if 0 == (solution.item.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
									begin
										work = Work.find($1)
										yield $1
										puts "#{viaf_id.ljust(26)} => #{title} :- #{$1}/#{work.label}"
									rescue ActiveRecord::RecordNotFound
										puts "#{viaf_id.ljust(26)} <= #{title} :- #{solutions.first.item.to_s}"
									end
								else
									STDERR.puts "??? multiple Database hits for #{$1}"
								end
							else
								STDERR.puts "??? multiple Wikidata hits for #{viaf_id}"
							end
						else
							STDERR.puts "??? unrecognised identifier #{viaf_id}"
						end
					else
					end
				end
			}
		end

		desc "test viaf xml parser with plato xml file"
		task plato_file: :environment do
			File.open('plato_indent.xml') {|f|
				parse_viaf(f)
			}
		end

	end # namespace viaf

end # namespace canon
