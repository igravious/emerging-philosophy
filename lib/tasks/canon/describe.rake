namespace :canon do

	desc "describely"
	task describe: :environment do

		require 'sparql/client'
		wikidata = Rails.configuration.x.canon.wikidata
		sparql = SPARQL::Client.new(wikidata)

		WORKS_QUERY = <<-SPARQL.chomp
#Describe an entity
DESCRIBE wd:%{interpolated_entity}
SPARQL

		all = Work.where(triple_count: nil)
		size = all.size
		bar = progress_bar(size, true)
		all.each {|work|
			begin
				solutions = sparql.query(WORKS_QUERY % {interpolated_entity: work.q})
				len = solutions.size
				work.triple_count = len
				work.save!
				update_progress(bar)
			rescue
				STDERR.puts $!
				STDERR.puts $!.backtrace.select {|l| l.to_s[File.basename(__FILE__)]}
				STDERR.puts work.inspect
				exit
			end
		}
	
	end
end
