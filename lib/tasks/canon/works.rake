namespace :canon do

	OPTIONAL=true

	#
	# What's happening here is that we're getting a list of philosophical things
	#
	# The genre is either a `subclass_of' `philosophy' or an `instance_of' a `branch_of_philosophy'
	#
	# wdt:P921 (genre)
	#
	# wdt:P279 wd:Q5891 (subclass_of philosophy)
	#
	# wdt:P31 wd:Q22811234 (instance_of branch_philosophy)
	#
	# Simultaneously populate the What table and the Author table

	desc "populate Work (works table) with specifically philosophical works"
	task facet2: :environment do

		require 'sparql/client'
		wikidata = Rails.configuration.x.canon.wikidata
		w = SPARQL::Client.new(wikidata)

		WORKS_QUERY = <<-SPARQL.chomp
#Works of type philosophy subclass-of philosophy or instance-of branch-of-philosophy
SELECT ?item ?itemLabel ?genre ?genreLabel ?author ?authorLabel ?linkCount WHERE
{ 
  {
    SELECT DISTINCT ?item ?genre ?author (COUNT(?sitelink) AS ?linkCount) WHERE {
      ?item wdt:P921 ?genre.
      ?genre wdt:P279 wd:Q5891.
      OPTIONAL {?item wdt:P50 ?author.}
      OPTIONAL {?sitelink schema:about ?item.}
    } GROUP BY ?item ?genre ?author
  }
  UNION
  {
    SELECT DISTINCT ?item ?genre ?author (COUNT(?sitelink) AS ?linkCount) WHERE {
      ?item wdt:P921 ?genre.
      ?genre wdt:P31 wd:Q22811234.
      OPTIONAL {?item wdt:P50 ?author.}
      OPTIONAL {?sitelink schema:about ?item.}
    } GROUP BY ?item ?genre ?author
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}
SPARQL

		solutions = w.query(WORKS_QUERY)
		len = solutions.length
		bar = progress_bar(len, true, 'entities')
		count = 1
		if len > 0
			solutions.each do |solution|
				begin
					# puts "processing #{count} of #{len}"
					update_progress(bar)
					if 0 == (solution[:item].to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
						begin
							work = Work.find($1)
							work.link_count = solution.linkCount.to_i
							work.save!
							#what(work, solution)
							author(work, solution, OPTIONAL)
							genre(work, solution)
						rescue ActiveRecord::RecordNotFound
							work = Work.new
							work.q = $1
							work.en_label = solution.itemLabel.to_s
							work.link_count = solution.linkCount.to_i
							begin
								work.save!
								#what(work, solution)
								author(work, solution, OPTIONAL)
								genre(work, solution)
							rescue
								STDERR.puts $!
								STDERR.puts "Duplicate (q) different type: #{solution.inspect}"
								STDERR.puts solution.inspect
								STDERR.puts work.inspect
							end
						end
					else
						STDERR.puts "Bad solution (item): #{solution.inspect}"
						exit
					end
					count += 1
				rescue
					STDERR.puts $!
					STDERR.puts $!.backtrace.select {|l| l.to_s[File.basename(__FILE__)]}
					STDERR.puts solution.inspect
					exit
				end
			end
		end

	end

	def genre(work, solution, optional=false)
		if not solution.bound?(:genre)
			if optional
				return
			else
				STDERR.puts "Not bound! (genre): #{solution.inspect}"
				exit
			end
		end
		if 0 == (solution.genre.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
			begin
				genre = Genre.find($1)
			rescue ActiveRecord::RecordNotFound
				genre = Genre.new
				genre.q = $1
				genre.en_label = solution.genreLabel.to_s
				genre.save!
			end
			begin
				genre_of = GenreOf.new
				#STDERR.puts work.inspect
				genre_of.genre_id = genre.q
				#STDERR.puts genre.inspect
				genre_of.of_id = work.q
				#STDERR.puts genre_of.inspect
				genre_of.save!
			rescue ActiveRecord::RecordNotUnique
				# SQLite3::ConstraintException
			end
		else
			STDERR.puts "Bad solution (genre): #{solution.inspect}"
			exit
		end
	end

	def author(work, solution, optional=false)
		if not solution.bound?(:author)
			if optional
				return
			else
				STDERR.puts "Not bound! (author): #{solution.inspect}"
				exit
			end
		end
		if 0 == (solution.author.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
			begin
				author = Author.find($1)
			rescue ActiveRecord::RecordNotFound
				author = Author.new
				author.q = $1
				author.en_label = solution.authorLabel.to_s
				author.save!
			end
			begin
				author_of = AuthorOf.new
				#STDERR.puts work.inspect
				author_of.author_id = author.q
				#STDERR.puts author.inspect
				author_of.of_id = work.q
				#STDERR.puts author_of.inspect
				author_of.save!
			rescue ActiveRecord::RecordNotUnique
				# SQLite3::ConstraintException
			end
		else
			STDERR.puts "Bad solution (author): #{solution.inspect}"
			exit
		end
	end

	def what(work, solution)
		if not solution.bound?(:what)
			# STDERR.puts "Not bound! (what): #{solution.inspect}"
			return
		end
		if 0 == (solution.what.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
			begin
				what = What.find($1)
			rescue ActiveRecord::RecordNotFound
				what = What.new
				what.q = $1
				what.en_label = solution.whatLabel.to_s
				what.save!
			end
			begin
				instance_of = InstanceOf.new
				#STDERR.puts work.inspect
				instance_of.instance_id = what.q
				#STDERR.puts what.inspect
				instance_of.of_id = work.q
				#STDERR.puts instance_of.inspect
				instance_of.save!
			rescue ActiveRecord::RecordNotUnique
				# SQLite3::ConstraintException
			end
		else
			STDERR.puts "Bad solution (what): #{solution.inspect}"
			exit
		end
	end



	# if there's an English label, use it
	# if not, try to figure out which label to use
	def guess_language(doc, work)
		l = {}
		en = false
	 	labels = doc.xpath('//entity/labels').first.children
		labels.each {|label|
			if 'en' == label['language']
				en = true
				l['en'] = label
			else
				l[label['language']] = label
			end
		}

	  sitelinks = doc.xpath('//entity/sitelinks').first.children
		biggest = 0
		which = ''
		sitelinks.each {|sitelink|
			#binding.pry
			site = sitelink['site']
			if not site.nil? and site.end_with?('wiki')
				lang=site[0..(site.length-5)]
				if l.key?(lang)
					url = "https://#{lang}.wikipedia.org/wiki/#{CGI.escape(sitelink['title'].gsub(' ','_'))}"
					begin
						f = open(url)
						if f.size > biggest
							biggest = f.size
							which = lang
						end
					rescue OpenURI::HTTPError
						STDERR.puts $!
						# do not complain
					rescue
						STDERR.puts $!
					end
				end
			end
		}
		work.size = biggest

		if en
			return l['en']
		else
			if l[which].nil?
				return labels.first
			else
				l[which]
			end
		end
	end

	# Work.where('en_label LIKE "Q1%" OR en_label LIKE "Q2%" OR en_label LIKE "Q3%" OR en_label LIKE "Q4%" OR en_label LIKE "Q5%" OR en_label LIKE "Q6%" OR en_label LIKE "Q7%" OR en_label LIKE "Q8%" OR en_label LIKE "Q9%"')

	desc "get Wikipedia info from Wikidata Id?"
	task wikipedia: :environment do

		# trap ctrl-C so it happens in the right place

		require 'open-uri'
		count = 1
		#works = Work.where('en_label LIKE "Q1%" OR en_label LIKE "Q2%" OR en_label LIKE "Q3%" OR en_label LIKE "Q4%" OR en_label LIKE "Q5%" OR en_label LIKE "Q6%" OR en_label LIKE "Q7%" OR en_label LIKE "Q8%" OR en_label LIKE "Q9%"')
		#works = Work.all
		works = Work.where(size: 0)
		size = works.size
		bar = progress_bar(size, true, 'entities')
		works.each do |work|
			begin
				#begin
				#	schema = Schema.find(work.q)
				#rescue ActiveRecord::RecordNotFound
				#	schema = Schema.new
				#	schema.wikidata_id = work.q
				#end
				doc = ''
				schema = Schema.where(wikidata_id: work.q).first
				if schema.nil?
					schema = Schema.new
					schema.wikidata_id = work.q
					open("https://www.wikidata.org/w/api.php?action=wbgetentities&format=xml&ids=#{work.q}") {|f|
						schema.wb = f.read
					}
					schema.save!
				end
				doc = Nokogiri::XML(schema.wb)
				label = guess_language(doc, work)
				if label.nil?
					work.en_label = work.q
					work.lang = ''
					work.label =''
				else
					lang = label['language']
					value = label['value']
					if 'en' == lang
						work.en_label = value
						work.lang = 'en'
						work.label = work.en_label
					else
						#p label
						work.en_label = work.q
						work.lang = lang
						work.label = value
					end
				end
				work.save!
				GC.start
				update_progress(bar)
			rescue
				STDERR.puts $!
				STDERR.puts $!.backtrace.select {|l| l.to_s[File.basename(__FILE__)]}
				STDERR.puts work.inspect
				exit
			end
		end
	end

	# https://stackoverflow.com/questions/37079989/how-to-get-wikipedia-page-from-wikidata-id
	desc "get sitelinks"
	task sitelinks: :environment do
		# https://www.wikidata.org/w/api.php?action=wbgetentities&format=xml&props=sitelinks&ids=Q42&sitefilter=enwiki
		#
		require 'open-uri'
		# require 'pry'

		count = 1
		works = Work.all
		size = works.size
		works.each do |work|
			# begin
				open("https://www.wikidata.org/w/api.php?action=wbgetentities&format=xml&props=sitelinks&ids=#{work.q}&sitefilter=enwiki") {|f|
					doc = Nokogiri::XML(f)
					wx = WorkExt.new
					wx.wikidata_id = work.q
					begin
						sitelink = doc.xpath('//sitelink').first
						if sitelink.nil?
							wx.underscore = ''
						else
							name = sitelink['title']
							if name.nil? or name.empty?
								wx.underscore = ''
							else
								wx.underscore = name.gsub(' ','_')
							end
						end
					rescue
						STDERR.puts doc
						exit
					end
					puts "#{count} of #{size}: #{wx.inspect}"
					# result_set = WorkExt.where(wikidata_id: wx.wikidata_id, underscore: wx.underscore)
					# if 0 == result_set.length
					begin
						wx.save!
					rescue ActiveRecord::RecordNotUnique
						# WorkExt.find(wx.wikidata_id)
					end
					# end
				}
			# rescue
				# STDERR.puts $!
				# binding.pry
			# end
			count += 1
		end
	end

	desc "get rid of undesireables"
	task expunge: :environment do
		begin
			one_by_one('Work', "SELECT ?is_a WHERE {wd:Q%{interpolated_entity} ?is_a wd:Q3331189.}", {link_count: :asc}) {|solution_set, work|
        if not solution_set.empty?
          #puts "#{work.id} #{work.entity_id} #{work.obsolete}"
					# => WhatOf(what_id: string, of_id: string, created_at: datetime, updated_at: datetime)
					# what_id: What
					# of_id: Work
					# => GenreOf(genre_id: string, of_id: string, created_at: datetime, updated_at: datetime)
					# genre_id: Genre
					# of_id: Work
					# => AuthorOf(author_id: string, of_id: string, created_at: datetime, updated_at: datetime)
					# author_id: Author
					# of_id: Work
					
					#WhatOf.where(of_id: work.q).delete_all
					p WhatOf.where(of_id: work.q)
					#GenreOf.where(of_id: work.q).delete_all
					p GenreOf.where(of_id: work.q)
					#AuthorOf.where(of_id: work.q).delete_all
					p AuthorOf.where(of_id: work.q)
          #work.destroy!
          p work
        end
      } 
		rescue
			STDERR.puts $!
			STDERR.puts $!.backtrace.select {|l| l.to_s[File.basename(__FILE__)]}
			exit
		end
	end

	desc "populate Work (works table) from Wikidata with works by philosophical figures"
	task facet1: :environment do

		require 'sparql/client'
		wikidata = Rails.configuration.x.canon.wikidata
		w = SPARQL::Client.new(wikidata)

		WORKS_QUERY = <<-SPARQL.chomp
#Works by philosophers
SELECT ?item ?itemLabel ?author ?authorLabel ?linkCount WHERE {
  {
	  SELECT ?item (COUNT(?sitelink) AS ?linkCount) ?author WHERE { 
			?item wdt:P50 ?author.
			?author wdt:P106 wd:Q4964182.
			OPTIONAL {?sitelink schema:about ?item.}
		} GROUP BY ?item ?author
	}
	SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
}
SPARQL

		solutions = w.query(WORKS_QUERY)
		len = solutions.length
		bar = progress_bar(len, true, 'entities')
		count = 1
		if len > 0
			solutions.each do |solution|
				begin
					#puts "processing #{count} of #{len}"
					update_progress(bar)
					if 0 == (solution.item.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
						begin
							work = Work.find($1)
							# what(work, solution)
							author(work, solution)
							# update
							work.en_label = solution.itemLabel.to_s
							work.link_count = solution.linkCount.to_i
							work.save!
						rescue ActiveRecord::RecordNotFound
							work = Work.new
							# create
							work.q = $1
							work.en_label = solution.itemLabel.to_s
							work.link_count = solution.linkCount.to_i
							begin
								work.save!
								author(work, solution)
								# what(work, solution)
							rescue
								# TODO
								STDERR.puts $!
								STDERR.puts "Duplicate (q) different type: #{solution.inspect}"
								STDERR.puts solution.inspect
								STDERR.puts work.inspect
							end
						end
					else
						STDERR.puts "Bad solution (item): #{solution.inspect}"
						exit
					end
					count += 1
				rescue
					STDERR.puts $!
					STDERR.puts $!.backtrace.select {|l| l.to_s[File.basename(__FILE__)]}
					STDERR.puts solution.inspect
					exit
				end
			end
		end

	end

	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	# I G N O R E
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#
	#

	desc "old facet"
	task old_facet: :environment do

		require 'sparql/client'
		wikidata = Rails.configuration.x.canon.wikidata
		w = SPARQL::Client.new(wikidata)

		WORKS_QUERY = <<-SPARQL.chomp
#Works by philosophers
#??
SPARQL

		solutions = w.query(WORKS_QUERY)
		len = solutions.length
		count = 1
		if len > 0
			solutions.each do |solution|
				puts "processing #{count} of #{len}"
				if 0 == (solution.item.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
					begin
						work = Work.find($1)
						STDERR.puts "Duplicate solution"
						STDERR.puts work.inspect
						STDERR.puts solution.inspect
					rescue ActiveRecord::RecordNotFound
						work = Work.new
						work.item = $1
						work.itemLabel = solution.itemLabel.to_s
						if 0 == (solution.author.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
							work.author = $1
							work.authorLabel = solution.authorLabel.to_s
							if 0 == (solution.what.to_s =~ /http:\/\/www.wikidata.org\/entity\/(.+)/)
								work.what = $1 # redundant
								work.whatLabel = solution.whatLabel.to_s
								work.name = solution.name.to_s
								work.save!
							else
								STDERR.puts "Bad solution (what): #{solution.inspect}"
								exit
							end
						else
							STDERR.puts "Bad solution (author): #{solution.inspect}"
							exit
						end
					end
				else
					STDERR.puts "Bad solution (item): #{solution.inspect}"
					exit
				end
				count += 1
			end
		end

	end

end
