namespace :canon do

	desc "Danker ranking!"
	task danker: :environment do
		fn = Dir[Rails.root.join('db','danker','**.rank')][-1]
		works = Work..order(link_count: :desc)
		size = works.size
		count = 1
		works.each { |work|
			puts "processing #{count} of #{size}"
			begin
				ent = work.q
        urk = `grep "#{ent}\t" #{fn}`
        q = "Q#{ent}".ljust(9)
        s = urk.split("\t")[1].to_f
        puts "#{q} #{s}"
				if not s.nil?
        	work.update(danker: s) # defaults to 0.0
				end
			rescue
				STDERR.puts $!
				STDERR.puts $!.backtrace.select {|l| l.to_s[File.basename(__FILE__)]}
				STDERR.puts work.inspect
				exit
			end
			count += 1
		}

  end

	desc "rank Work (works table) using DBpedia PageRank data"
	task dbpedia_rank: :environment do

		require 'sparql/client'
		dbpedia = Rails.configuration.x.canon.dbpedia
		sparql = SPARQL::Client.new(dbpedia)

		# https://idiosyncratic-ruby.com/49-what-the-format.html
		RANK_QUERY = <<-SPARQL.chomp
PREFIX v:<http://purl.org/voc/vrank#>

SELECT ?r

FROM <http://dbpedia.org>
FROM <http://people.aifb.kit.edu/ath/#DBpedia_PageRank>

WHERE
{
<http://dbpedia.org/resource/%s> owl:sameAs <http://www.wikidata.org/entity/%s>;
v:hasRank/v:rankValue ?r.
}
SPARQL

		works = Work.all
		size = works.size
		count = 1
		works.each { |work|
			puts "processing #{count} of #{size} – (#{work.rank})"
			if work.rank.nil?
				escaped = URI.escape(work.underscore)
				rank_query = RANK_QUERY % [escaped, work.item]
				solutions = sparql.query(rank_query)
				len = solutions.length
				if 1 != len
					STDERR.puts "Bad solution with `#{escaped}' for #{work.item}"
					STDERR.puts solutions.inspect
				else
					r = solutions.first.r
					# require 'pry'
					# binding.pry
					work.rank = r.to_s.to_f
					work.save
				end
			end
			count += 1
		}
	end

end
