namespace :playground do

	desc "let us play"
	task sandbox: :environment do
		['goos', 'foos'].each do |table_name|
			r = Record.where(set: 'Playground', table_name: table_name)
			if 0 == r.size
				r = Record.new
				r.set = 'Playground'
				r.table_name = table_name
				r.save
			end
		end
	end
end
