
#                config.x.zotero.raw_data_dir = 'tmp/raw_data/'
#                config.x.zotero.key_file = 'config/zotero.key'
#                config.x.zotero.item_type = :item
#                config.x.zotero.prefix = 'Q' # surprise, surprise :/
#                config.x.zotero.suffix = 'X'
#                config.x.zotero.collection = 'emerging philosophy'
#                config.x.zotero.id = 1248965
#                config.x.zotero.the_works = '_zotero_works.txt'
#                config.x.zotero.the_attachments = '_zotero_attachments.txt'
#                config.x.zotero.the_entries = '_zotero_entries.txt'

namespace :zotero do
	def go_fetch
		key_file = Rails.configuration.x.zotero.key_file
		id = Rails.configuration.x.zotero.id
		key = File.read(key_file)
		# puts key
		# puts id

		require 'zotero'

		library = Zotero::Library.new id, key

		collections = library.collections

		collection = collections.select{|x|x.name == 'emerging philosophy'}.first

		entries = collection.entries

		works = collection.works

		attachments = collection.attachments

		puts "entries: #{entries.length}"
		puts "“works”: #{works.length}"
		puts "attachments: #{attachments.length}"

		# require 'pry'

		# binding.pry
		
		boom = Time.now.strftime("%Y%m%d%H%M%S")

		dir = Rails.configuration.x.zotero.raw_data_dir
		fn_entries = Rails.root.join(dir, boom+Rails.configuration.x.zotero.the_entries)
		p fn_entries
		fn_works = Rails.root.join(dir, boom+Rails.configuration.x.zotero.the_works)
		p fn_works
		fn_attachments = Rails.root.join(dir, boom+Rails.configuration.x.zotero.the_attachments)
		p fn_attachments

		File.open(fn_entries, 'w') { |f|
			entries.each { |e|
				f.write(e.to_json+"\n")
			}
		}
		File.open(fn_works, 'w') { |f|
			works.each { |w|
				f.write(w.to_json+"\n")
			}
		}
		File.open(fn_attachments, 'w') { |f|
			attachments.each { |a|
				f.write(a.to_json+"\n")
			}
		}
	end

	desc "Grab all items (entries/works/attachments) from Zotero store, save as JSON in `tmp/raw_data'"
  task fetch: :environment do
		go_fetch
  end
end
