namespace :zotero do

	require 'record_helper'

	def assign(model1, model2)
		fqmn = model2.class
		row_assoc = mdl2row(fqmn)
		model1.send((row_assoc+'=').to_sym, model2)
	end

	def retrieve(model1, fqmn)
		row_assoc = mdl2row(fqmn) 
		model1.send(row_assoc.to_sym)
	end

	def unique(rec)
		rec.save
		rec
	rescue ActiveRecord::RecordNotUnique
		model = rec.class
		columns = model.attribute_names - [model.primary_key, "created_at", "updated_at"]
		attrs = {}
		columns.each do |column|
			attrs[column.to_sym] = rec[column]
		end
		rec = model.where(attrs)
		if 1 != rec.length
			raise "MultipleRecordProblem #{rec.inspect}"
		else
			rec.first
		end
	end
			# permitted = (fqmn.attribute_names - [fqmn.primary_key, "created_at", "updated_at"]).collect{|word| ':'+word}.inject {|memo, word| memo+', '+word}

	def primary_secondary(the_type, the_key)
		type = the_type.to_sym          				          # => :journalArticle
		type_name = type.to_s.underscore                  # => "journal_article"
		fqmn = prefixed_const(type_name.camelcase)        # => Q::JournalArticle
		ext = type_name_ext(type)                         # => "journal_article_x"

		it = Rails.configuration.x.zotero.item_type       # => :item
		_I = prefixed_const(it.to_s.camelcase)            # => Q::Item
		i_row_name = mdl2row(_I)                          # => q_item

		fqmn_ext = prefixed_const(ext.camelcase)          # => Q::JournalArticleX
		# row_name_ext = mdl2row(fqmn_ext)                  # => "q_journal_article_x"

		if (primary = fqmn.where(key: the_key).first).nil?
			primary = fqmn.new
			secondary = fqmn_ext.new
			secondary.save # small bit redundant
			assign(primary, secondary)
			primary.save   # but make them available
		else
			secondary = retrieve(primary, fqmn_ext)
		end
		[primary, secondary]
	end

	# Prepare for merging. Delete Zotero-origin'd entities, entity happenings,
	# dates, date happenings, tags and tagged items.
	desc 'Prepare for merging – Delete Zotero-origin\'d data that\'ll be overwritten.'
	task prepare: :environment do
		_V, _H, _E, _EH, _D, _DH, _T, _TI, _S = prefixed_consts

		# _V is okay, if it wasn't things would be real bad

		# TODO prefixify'd code
		s = Q::Source.where(where: 'Zotero').first
		Q::TaggedItem.where(q_source_id: s.id).delete_all
		Q::Tag.all.each { |tag|
			dangling = Q::TaggedItem.where(q_tag_id: tag.id).size
			tag.destroy if 0 == dangling
		}
		happy = Q::Happening.where(q_source_id: s.id)
		Q::DateHappening.where(q_happening_id: happy.pluck('id')).delete_all
		Q::EntityHappening.where(q_happening_id: happy.pluck('id')).delete_all
		Q::TheDate.all.each { |the_date|
			dangling = Q::DateHappening.where(q_the_date_id: the_date.id).size
			the_date.destroy if 0 == dangling
		}
		Q::Entity.all.each { |entity|
			dangling = Q::EntityHappening.where(q_entity_id: entity.id).size
			entity.destroy if 0 == dangling
		}
		happy.delete_all
	end

	desc 'Merge all the yummy Zotero data'
	task merge: :environment do

		Rails.logger.warn ActiveSupport::LogSubscriber.new.send(:color, '================', :yellow)
		Rails.logger.warn ActiveSupport::LogSubscriber.new.send(:color, '!Here be Tygers!', :yellow)
		Rails.logger.warn ActiveSupport::LogSubscriber.new.send(:color, '================', :yellow)

		freq_attrs, item_types, attrs_in, tables, works = shared_parse

		primary_map = {
			'dateModified' => 'updated_at',
			'dateAdded' => 'created_at',
			'key' => 'key',
			'title' => 'title',
			'abstractNote' => 'description',
			'shortTitle' => 'short_title',
			'url' => 'url',   # TODO? URL
			'rights' => 'rights',
			'extra' => 'extra'
		}

		secondary_map = {
			'ISSN' => 'issn', # TODO? ISSN
			'ISBN' => 'isbn', # TODO? ISBN
			'DOI' => 'doi'    # TODO? DOI
		}

		creator_type_map = {
			'author' => 'author', # author is both a verb and a noun
			'editor' => 'edit',
			'contributor' => 'contribute',
			'programmer' => 'program'
		}

		# -rw-rw-r--. 1 anthony developers 117 May  4 02:06 blog_post.rb
		# -rw-rw-r--. 1 anthony developers  90 May  7 11:36 blog_post_x.rb
		# -rw-rw-r--. 1 anthony developers 126 May  4 02:06 book_section.rb
		# -rw-rw-r--. 1 anthony developers  93 May  7 11:36 book_section_x.rb
		# -rw-rw-r--. 1 anthony developers 104 May  7 11:34 book.rb
		# -rw-rw-r--. 1 anthony developers  86 May  7 11:36 book_x.rb
		# -rw-rw-r--. 1 anthony developers 138 May  7 11:35 computer_program.rb
		# -rw-rw-r--. 1 anthony developers  97 May  7 11:36 computer_program_x.rb
		# -rw-rw-r--. 1 anthony developers 138 May  4 02:06 conference_paper.rb
		# -rw-rw-r--. 1 anthony developers  97 May  7 11:36 conference_paper_x.rb
		# -rw-rw-r--. 1 anthony developers 150 May  4 02:06 encyclopedia_article.rb
		# -rw-rw-r--. 1 anthony developers 101 May  7 11:36 encyclopedia_article_x.rb
		# -rw-rw-r--. 1 anthony developers 135 May  7 11:34 journal_article.rb
		# -rw-rw-r--. 1 anthony developers  96 May  7 11:36 journal_article_x.rb
		# -rw-rw-r--. 1 anthony developers 141 May  4 02:06 newspaper_article.rb
		# -rw-rw-r--. 1 anthony developers  98 May  7 11:36 newspaper_article_x.rb
		# -rw-rw-r--. 1 anthony developers 110 May  4 02:06 report.rb
		# -rw-rw-r--. 1 anthony developers  88 May  7 11:36 report_x.rb
		# -rw-rw-r--. 1 anthony developers 113 May  4 02:06 webpage.rb
		# -rw-rw-r--. 1 anthony developers  89 May  7 11:36 webpage_x.rb
		#
		# -rw-rw-r--. 1 anthony developers  38 May  3 17:39 item.rb
		# -rw-rw-r--. 1 anthony developers  38 May  3 17:12 verb.rb
		# -rw-rw-r--. 1 anthony developers 137 May  3 17:12 happening.rb
		# -rw-rw-r--. 1 anthony developers  40 May  3 17:12 entity.rb
		# -rw-rw-r--. 1 anthony developers 157 May  3 17:12 entity_happening.rb
		# -rw-rw-r--. 1 anthony developers  41 May  3 17:12 the_date.rb
		# -rw-rw-r--. 1 anthony developers 158 May  3 17:12 date_happening.rb
		# -rw-rw-r--. 1 anthony developers  37 May  3 17:12 tag.rb
		# -rw-rw-r--. 1 anthony developers 136 May  3 17:12 tagged_work.rb
		_V, _H, _E, _EH, _D, _DH, _T, _TI, _S = prefixed_consts
		# _I is initialised separately just below

		s = _S.where(where: 'Zotero').first
		it = Rails.configuration.x.zotero.item_type       # => :item
		_I = prefixed_const(it.to_s.camelcase)            # => Q::Item
		count = 0
		works.each do |work|
			primary, secondary = primary_secondary(work['itemType'], work['key'])

			work.each do |attr, value|
				if freq_attrs[attr] == works.length
					if 'itemType' == attr
						# already dealt with
					elsif 'date' == attr
						d = _D.new
						the_date = work['date']
						d.when = the_date
						if d.when.nil? # just the year
							d.year = the_date
							d.month = -1
							d.day = -1
						else
							parsed = Time.parse(the_date).to_date
							d.year = parsed.year
							d.month = parsed.month
							if the_date.include?(':') # full timestamp
								d.when = parsed
								d.day = parsed.day
							elsif the_date =~ (/\d\d /) # day, month, year
								d.day = parsed.day
							else # day, month
								d.day = -1
								d.when = nil
							end
						end
						d = unique(d)

						v = _V.new
						v.lexeme = 'publish'
						v = unique(v)

						h = _H.new # h = vi
						assign(h, primary.becomes(_I))
						assign(h, v)
						assign(h, s)
						h = unique(h)

						dh = _DH.new
						assign(dh, d)
						dh.prep = 'on'
						assign(dh, h)
						dh = unique(dh)

						primary['date'] = the_date # is this needed ?
					elsif 'tags' == attr
						work['tags'].each { |tag_hash|
							t = _T.new
							t.label = tag_hash['tag']
							t = unique(t)

							ti = _TI.new # ti instead of vi
							assign(ti, primary.becomes(_I))
							assign(ti, t)
							assign(ti, s)
							ti = unique(ti)
						}
					elsif 'creators' == attr
						# {"creatorType"=>"author", "firstName"=>"Patrick", "lastName"=>"Grim"},
						# {"creatorType"=>"author", "firstName"=>"Daniel", "lastName"=>"Singer"},
						# {"creatorType"=>"editor", "firstName"=>"Edward N.", "lastName"=>"Zalta"}
						work['creators'].each { |creator_hash|
							e = _E.new
							if creator_hash['firstName'].nil? and creator_hash['lastName'].nil?
								e.name = creator_hash['name']
							else
								e.first_name = creator_hash['firstName']
								e.last_name = creator_hash['lastName']
								e.name = creator_hash['firstName']+' '+creator_hash['lastName']
							end
							e = unique(e)

							v = _V.new
							type = creator_hash['creatorType']
							if creator_type_map.has_key?(type)
								v.lexeme = creator_type_map[type]
							else
								binding.pry
							end
							v = unique(v)

							h = _H.new # h = vi
							assign(h, primary.becomes(_I))
							assign(h, v)
							assign(h, s)
							h = unique(h)

							eh = _EH.new
							assign(eh, e)
							eh.prep = 'by'
							assign(eh, h)
							eh = unique(eh)
						}
					else
						begin
							# puts "in #{_I}##{attr} for #{fqmn}"
							the_attr = primary_map[attr]
							primary[the_attr] = value
							# puts "in #{_I}##{the_attr} for #{fqmn}"
						rescue ActiveModel::MissingAttributeError
							# skip some Zotero usage specific ones
							puts "#{$!} (for `#{attr}')" unless ['version', 'accessDate', 'relations', 'collections'].include?(attr)
						end
					end
				else
					begin
						# puts "in #{fqmn_ext}##{attr}"
						the_attr = secondary_map[attr]
						if the_attr.nil?
							the_attr = attr.underscore
						end
						secondary[the_attr] = value
						# puts "in #{fqmn_ext}##{the_attr}"
					rescue ActiveModel::MissingAttributeError
						# don't skip any
						puts "#{$!} (for `#{attr}')"
					end
				end
			end
			# puts "primary (after)"
			# p primary
			# puts "secondary (after)"
			# p secondary
			# puts "==="
			
			# TODO progress bar
			begin
				primary.save
				secondary.save
			rescue
				# type has changed!
				i = _I.where(key: primary.key).first
				old_primary, old_secondary = primary_secondary(i.type, i.key)
				old_secondary.delete
				old_primary.delete
				primary.save
				secondary.save
			end
			count += 1
		end
		puts "processed #{count} of #{works.length}"

	end

end
