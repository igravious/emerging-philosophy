namespace :zotero do
	# https://stackoverflow.com/questions/7044714/whats-the-environment-task-in-rake
	# “You can get access to your models, and in fact, your whole environment by making tasks dependent
	#  on the environment task. This lets you do things like `run rake RAILS_ENV=staging db:migrate`.”
  desc "TODO (make.rake)"
  task make: [:makegen, :makedel] do
  end

  desc "TODO"
  task makegen: :environment do
  end

  desc "TODO"
  task makedel: :environment do
  end
end
