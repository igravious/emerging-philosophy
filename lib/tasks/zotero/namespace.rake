namespace :zotero do

	def invoke(invoked)
		STDOUT.puts '      '+'invoke'.bold+'  '+invoked.to_s
	end

	def create(created)
		STDOUT.puts '      '+'create'.light_green+'    '+created.to_s
	end

	require 'record_helper'

	def controllers
		fn = 'api_record_controller.rb.erb'
		dir = File.dirname(__FILE__)
		str = File.read(File.join(dir, fn))
		require 'erb'

		# bin/rails zotero:make seeds this table
		['Zotero', 'Funfair', 'Playground'].each do |set_name|
			set = Record.where(set: set_name)
			set.each do |record|
				table_name = record.table_name
				fqmn = tbl2mdl(table_name)
				model_name = fqmn.to_s.demodulize
				squished_name = table_name.camelcase # it's the Rails file name to class name naming convention
				row_name = table_name.singularize
				# https://ruby-doc.org/core-2.6.3/Enumerable.html#method-i-reduce
				permitted = (fqmn.attribute_names - [fqmn.primary_key, "created_at", "updated_at"]).collect{|word| ':'+word}.inject {|memo, word| memo+', '+word}
				# https://ruby-doc.org/stdlib-2.6.3/libdoc/erb/rdoc/ERB.html
				template = ERB.new(str)
				# bin/rails g controller Api::QEntities
				# app/controllers/api/q_entities_controller.rb
				joined = Rails.root.join('app', 'controllers', 'api', "#{table_name}_controller.rb")
				if File.exists?(joined)
					File.write(joined, template.result(binding))
					create(joined)
				end
			end
		end
	end

	def models
		freq_attrs, item_types, attrs_in, tables, works = shared_parse
		item_types.each do |type|                           # => :journalArticle
			type_name = type.to_s.underscore                  # => "journal_article"
			ext = type_name_ext(type)                         # => "journal_article_x"

			it = Rails.configuration.x.zotero.item_type       # => :item
			i_fqmn = prefixed_const(it.to_s.camelcase)        # => Q::Item
			i_row_name = mdl2row(i_fqmn)                      # => q_item

			_prefix = Rails.configuration.x.zotero.prefix.underscore
			joined = Rails.root.join('app', 'models', _prefix, "#{ext}.rb")
			# app/models/q/encyclopedia_article_x.rb
			# belongs_to :q_item\n
			# belongs_to :q_item, :class_name => 'Q::Item'\n
			# puts joined
			if File.exists?(joined)
				str = File.read(joined)
				match = "belongs_to :#{i_row_name}"
				replace = "#{match}"
				str.sub!(/#{match}\n/, "#{match}, :class_name => '#{i_fqmn}'\n")
				File.write(joined, str)
				create(joined)
			end

			fqmn_ext = prefixed_const(ext.camelcase)          # => Q::JournalArticleX
			row_name_ext = mdl2row(fqmn_ext)                  # => "q_journal_article_x"

			joined = Rails.root.join('app', 'models', _prefix, "#{type_name}.rb")
			# app/models/q/encyclopedia_article.rb
			# has_one :q_encyclopedia_article_x, :class_name => 'Q::EncyclopediaArticleX', :foreign_key => 'q_item_id'
			# puts joined
			if File.exists?(joined)
				str = File.read(joined)
				if 2 == str.lines.length
					l0 = str.lines[0]
					l1 = "  has_one :#{row_name_ext}, :class_name => '#{fqmn_ext}', :foreign_key => '#{i_row_name}_id'\n"
					l2 = str.lines[1]
					File.write(joined, l0+l1+l2)
					create(joined)
				end
			end
		end
	end

	desc "controllers and models"
	task namespace: :environment do
		invoke('controllers')
		controllers
		invoke('models')
		models
	end
end
