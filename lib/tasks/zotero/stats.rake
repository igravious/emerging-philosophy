namespace :zotero do

	require 'record_helper'

	desc "Get yer statistics, hot off the press"
	task record_stats: :environment do
		_V, _H, _E, _EH, _D, _DH, _T, _TW, _S = prefixed_consts

		it = Rails.configuration.x.zotero.item_type       # => :item
		_I = prefixed_const(it.to_s.camelcase)            # => Q::Item

		i = _I.all.size
		puts "#{i} items in total"
		# simple sanity check
		tables = Record.where(set: 'Zotero').pluck(:table_name)
		count = 0
		tables.each do |table|
			model = tbl2mdl(table)
			# require 'pry'
			# binding.pry
			if model.superclass == _I
				c = model.all.size
				puts "#{model} has #{c}"
				count += c
			else
			end
		end
		if count == i
			puts "basic sanity check passed!"
		else
			puts "total of #{i} does not match count #{count}"	
		end
		puts "==="
		puts "#{_V.all.size} distinct Verbs => #{_H.all.size} distinct Happenings (Item Verbs)"
		puts "==="
		puts "#{_E.all.size} separate Entities"
		puts "#{_EH.all.size} distinct Entity Happenings"
		puts "==="
		puts "#{_D.all.size} distinct Dates"
		puts "#{_DH.all.size} distinct Date Happenings"
		puts "==="
		puts "#{_T.all.size} distinct Tags"
		puts "#{_TW.all.size} separate Tags used in Items"
		puts "==="
		puts "#{_S.all.size} distinct Sources"
	end
end
