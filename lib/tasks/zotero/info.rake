namespace :zotero do
  desc "What are the ol' names?"
  task :args, [:param1] => :environment do |task, args|
		puts "task: #{task.inspect}"
		puts "args: “#{args}”"
  end

  desc "What are the ol' names?"
  task :info, [:name] => :environment do |task, args|
		if args[:name].nil? or args[:name].empty?
			puts Q::Entity.group(:name).count.sort_by {|k, v| -v}.to_h
		else
			name = args[:name]
			ents = Q::Entity.where('name LIKE ?', "%#{name}%").order(:name)
			ents.each do |ent|
				ent_haps = Q::EntityHappening.where(q_entity_id: ent.id)
				puts "==="
				p ent
				ent_haps.each do |ent_hap|
					hap = Q::Happening.find(ent_hap.q_happening_id)
					p Q::Item.find(hap.q_item_id)
				end
			end
		end
  end
end
