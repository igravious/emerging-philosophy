
import Vue from 'vue/dist/vue.esm' // template thingy (dev mode)

// https://vuejs.org/v2/guide/components-registration.html
// https://stackoverflow.com/questions/49053882/how-to-define-css-styles-for-a-vue-js-component-when-registering-that-component

require('stylesheets/foos.css')

import TableOfRecords from './components/table-of-records'

import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)

import Loggy from './plugins/loggy-vue'
Vue.use(Loggy)

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
		data: function () {
			return {
				mountedT: false,
				numberT: 1
			}
		},
  	el: '.app-entrypoint',
    components: { 'table-of-records': TableOfRecords },
		mounted: function() {
		},
		methods: {
			tableMounted: function(table) {
				table.notificationData.noticeLevel = 1
				table.getRecords()
				this.mountedT = true
			}
		}
	})
})
