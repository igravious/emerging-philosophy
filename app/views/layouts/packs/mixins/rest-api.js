
import axios from 'axios'

var RestApiMixin = {
	methods: {
		//
		// API functions
		//
		statusText: function(error, verb) {
			// console.log(error.response)
			var statusWindow = window.open("", "MsgWindow", "width=1010,height=505");
			statusWindow.document.write(error.response.data);
			return error.response.status+' ('+error.response.statusText+") Error in "+verb+": See logs for details."
		},

		// (1)             api_foos GET    /api/foos(.:format)                                                                      api/foos#index
		// (2)                      POST   /api/foos(.:format)                                                                      api/foos#create
		// (3)              api_foo GET    /api/foos/:id(.:format)                                                                  api/foos#show
		// (4)                      PATCH  /api/foos/:id(.:format)                                                                  api/foos#update
		// (5)                      PUT    /api/foos/:id(.:format)                                                                  api/foos#update
		// (6)                      DELETE /api/foos/:id(.:format)                                                                  api/foos#destroy

		// (1)
		//
		// GET /api/$records
		index: function(slot, callback, filter) {
			var self = this;
			//self.notificationMethods('debug', slot)
			var res = axios.get(slot, filter)
				.then(function (response) {
				callback(response.data, undefined)
			})
			.catch(function (error) {
				//self.notificationMethods('danger', error);
				callback(undefined, self.statusText(error, 'index'))
			})
			.finally(function () {
				self.notificationMethods('debug', 'completed')
				// always executed ?
			});
			// should we return res
		},

		// (2)
		//
		// POST /api/$records
		create: function(slot, callback, record) {
			var self = this;
			//self.notificationMethods('debug', slot)
			axios.post(slot, record)
			.then(function (response) {
				// new record success path!
				callback(response.data, undefined);
			})
			.catch(function (error) {
				// oops, something went wrong!
				//self.notificationMethods('danger', error);
				callback(undefined, self.statusText(error, 'create'))
			})
		},

		// (3)
		//
		// GET /api/$records/:id
		show: function(slot, callback, id) {
			var self = this;
			//self.notificationMethods('debug', slot)
			axios.get(slot+'/'+id)
			.then(function (response) {
				// update record success path!
				callback(response.data, undefined);
			})
			.catch(function (error) {
				// oops, something went wrong!
				//self.notificationMethods('danger', error);
				callback(undefined, self.statusText(error, 'update'))
			})
		},

		// (4) (5)
		//
		// PUT /api/$records/:id
		update: function(slot, callback, record) {
			var self = this;
			//self.notificationMethods('debug', slot)
			// Unpermitted parameter: :id
			axios.put(slot+'/'+record.id, record)
			.then(function (response) {
				// update record success path!
				callback(response.data, undefined);
			})
			.catch(function (error) {
				// oops, something went wrong!
				//self.notificationMethods('danger', error);
				callback(undefined, self.statusText(error, 'update'))
			})
		},

		// (6)
		//
		// DELETE /api/$records/:id
		destroy: function(slot, callback, id) {
			var self = this;
			//self.notificationMethods('debug', slot)
			axios.delete(slot+'/'+id)
			.then(function (response) {
				// delete record success path!
				callback(response.data, undefined);
			})
			.catch(function (error) {
				// oops, something went wrong!
				//self.notificationMethods('danger', error)
				callback(undefined, self.statusText(error, 'destroy'))
			})
		}
	}
}

export default RestApiMixin;
