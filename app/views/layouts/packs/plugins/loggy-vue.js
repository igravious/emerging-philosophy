
/*
module.exports = {
	install: function (Vue, options) {
		console.log('in install')
		Vue.myGlobalMethod = function () {
			return 'global method'
		},
		Vue.prototype.$myMethod = function (methodOptions) {
			return 'instance method'
		}
	}
};
*/

import StackTrace from 'stacktrace-js'

var Logger = {
	logItems: [],
	logToConsole: true,
}

// This is your plugin object. It can be exported to be used anywhere.
const MyPlugin = {
  // The install method is all that needs to exist on the plugin object.
  // It takes the global Vue object as well as user-defined options.
  install(Vue, options) {
    // We call Vue.mixin() here to inject functionality into all components.
		if (options && options.noisy) {
			console.debug('#install() installing loggy plugin …')
		}
		Vue.$myGlobalMethod = function () {
			return 'global method'
		}
		Vue.$passThru = function (fromObjOrFn, fromKey, toObj, toKey) {
			if (typeof fromObjOrFn === 'function') {
				// use its prototype if it's a fn() !
				fromObjOrFn = fromObjOrFn.prototype
			} else if (typeof fromObjOrFn === 'object') {
			} else {
				console.warn('Vue.$passthru called on non-object and non-function with key: '+fromKey)
				return
			}

			var property = Object.getOwnPropertyDescriptor(fromObjOrFn, fromKey);
			if (property && property.configurable === false) {
				console.warn('Vue.$passthru called on non-configurable property with key:'+fromKey)
				return
			}

			// cater for pre-defined getter/setters
			var getter = property && property.get;
			var setter = property && property.set;
			Object.defineProperty(fromObjOrFn, fromKey, {
      	enumerable: true,
      	configurable: true,
      	get: function passThruGetter () {
					//console.warn('Vue.$passthru get() with key: '+fromKey)
        	var value = getter ? getter.call(fromObjOrFn) : toObj[toKey];
					return value
				},
				set: function passThruSetter (newVal) {
					//console.warn('Vue.$passthru set() with key: '+fromKey)
					var value = getter ? getter.call(fromObjOrFn) : toObj[toKey];
					/* eslint-disable no-self-compare */
					if (newVal === value || (newVal !== newVal && value !== value)) {
						return
					}
					// #7981: for accessor properties without setter
					if (getter && !setter) { return }
					if (setter) {
						setter.call(fromObjOrFn, newVal);
					} else {
						toObj[toKey] = newVal;
					}
				}
			})
			return fromObjOrFn
		}
		// Q: Why would you need an instance method?
		// A: When you don't have access to the Vue object
		Vue.prototype.$myInstanceMethod = function (methodOptions) {
			return 'instance method'
		}
		/*
		Object.defineProperty(Vue.prototype, '$logItems', {
			get() { return Logger.logItems } // can modify this cuz we don't assign to it, we modify what it points to
		})
		Vue.$passThru(Vue, '$logToConsole', Logger, 'logToConsole')
		*/
		Object.defineProperty(Vue.prototype, '$logger', {
			get() { return Logger } // can modify this cuz we don't assign to it, we modify what it points to
		})
    // Anything added to a mixin will be injected into all components.
  	Vue.mixin({
			// Namespaced data
			data: function() {
				return {
					notificationData: {
						noticeLevel: 2,
					}
				}
			},
			// Namespaced methods
			// https://stackoverflow.com/questions/36108953/is-it-possible-to-nest-methods-in-vue-js-in-order-to-group-related-methods
   		methods: {
     		notificationMethods(name, msg) {
       		// you can access reactive data via `that` reference,
       		// from inside your functions
       		const _self = this

       		return {
						// user feedback functions (duplication cuz of namespacing parameter)
						showNotice3(msg) {
							// level is 3
							if (3 >= _self.notificationData.noticeLevel) {
								_self.notificationMethods('logNotice', {class: 'danger', message: msg})
							}
						},
						showNotice2(msg) {
							// level is 2
							if (2 >= _self.notificationData.noticeLevel) {
								_self.notificationMethods('logNotice', {class: 'warning', message: msg})
							}
						},
						showNotice1(msg) {
							// level is 1
							if (1 >= _self.notificationData.noticeLevel) {
								_self.notificationMethods('logNotice', {class: 'info', message: msg})
							}
						},
						showNotice(msg) {
							// level is unimportant
								_self.notificationMethods('logNotice', {class: 'success', message: msg})
						},
						logNotice(payload) {
								_self.$set(_self.$logger.logItems, _self.$logger.logItems.length, {class: payload.class, message: payload.message, active: true, component: _self})
						},
						dismissNotice(idx) {
							// msg unused
						},

						// console logging wrappers – https://v4-alpha.getbootstrap.com/components/alerts/
         		'danger': function (msg) {
							_self.notificationMethods('showNotice3', '<b>Oh snap!</b> '+msg)
							if (_self.$logger.logToConsole) {
								let fn = StackTrace.getSync()[2].functionName
								console.error("#"+fn+"() "+msg)
							} else {
								// ?
							}
         		},
         		'warning': function (msg) {
							_self.notificationMethods('showNotice2', '<b>Warning!</b> '+msg)
							if (_self.$logger.logToConsole) {
								let fn = StackTrace.getSync()[2].functionName
								console.warn("#"+fn+"() "+msg)
							} else {
								// ?
							}
         		},
         		'info': function (msg) {
							_self.notificationMethods('showNotice1', '<b>Heads up!</b> '+msg)
							if (_self.$logger.logToConsole) {
								let fn = StackTrace.getSync()[2].functionName
								console.info("#"+fn+"() "+msg)
							} else {
								// ?
							}
         		},
         		'success': function (msg) {
							_self.notificationMethods('showNotice', '<b>Well done!</b> '+msg)
							if (_self.$logger.logToConsole) {
								let fn = StackTrace.getSync()[2].functionName
								console.info("#"+fn+"() "+msg)
							} else {
								// ?
							}
         		},
						'debug': function (msg) {
							if (_self.$logger.logToConsole) {
								let fn = StackTrace.getSync()[2].functionName
								console.debug("#"+fn+"() "+msg)
							} else {
								// ?
							}
						}
       		}[name](msg)
     		}
   		}
    });
  }
};

export default MyPlugin;
