
import Vue from 'vue/dist/vue.esm' // template thingy (dev mode)

// https://vuejs.org/v2/guide/components-registration.html
// https://stackoverflow.com/questions/49053882/how-to-define-css-styles-for-a-vue-js-component-when-registering-that-component

require('stylesheets/records.css')
// require "stylesheets/#{table-name}.css" dynamically!

import TableOfRecords from './components/table-of-records'

import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)

import Loggy from './plugins/loggy-vue'
Vue.use(Loggy)

import RestApiMixin from './mixins/rest-api'

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
		mixins: [RestApiMixin],
		data: function () {
			// Vue.$passThru(o, 'god', this.$logger, 'logToConsole')
			return {
				numberT: 0,
				trackT: 0,
				henry: this.$logger,
			}
		},
		computed: {
			mountedT() {
				return !this.numberT
			}
		},
		el: '.each-entrypoint',
    components: { 'table-of-records': TableOfRecords },
		mounted: function() {
			for (let child in this.$children) {
				let tag = this.$children[child].$options._componentTag
				//console.info(tag)
				if (tag == 'table-of-records') {
					this.numberT += 1
					this.trackT += 1
				}
			}
		},
		methods: {
			//
			// API functions are in mixin
			//

			//
			// non API functions
			//
			apiTrigger: function(payload) {
				//console.warn(payload)
				this[payload.fn](payload.slot, payload.callback, payload.data)
			},
			tableMounted: function(table) {
				table.notificationData.noticeLevel = 1
				table.getRecords()
				this.numberT -= 1
				this.notificationMethods('debug', "===> "+this.refName(this.numberT))
				if (this.numberT) {
				} else {
					this.notificationMethods('debug', "loaded all the records")
				}
			},

			refName(idx) {
				return 'T'+idx
			},

			noticeActive(idx) {
				return this.$refs[this.refName(idx)].notificationData.noticeActive
			},

			noticeStyle(idx) {
				return this.$refs[this.refName(idx)].notificationData.noticeStyle
			},

			noticeMessage(idx) {
				return this.$refs[this.refName(idx)].notificationData.noticeMessage
			},

			dismissNotice(idx) {
				this.$refs[this.refName(idx)].notificationMethods('dismissNotice')
			},
		}
	})
})

/*
			blurFoo: function(self, attr, ev) {
				self = this;
				let f = self.attrs[attr]
				let o = ''
				if (f !== null && f !== undefined) {
					o = f.trim()
				}
				let n = ev.target.innerText.trim()
				self.log('blur')
				if(o != n) {
					self.attrs[attr] = n
					self.dirty = true
				}
				return true
			},
			paintFoo: function(foo) {
				if (foo.attrs.id === undefined && !foo.dirty) {
					return 'foo-record-new'
				} else {
					return ''
				}
			}
*/
