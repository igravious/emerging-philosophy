
import Vue from 'vue/dist/vue.esm'

import LI_Node from './components/li-node.vue'

// https://vuejs.org/v2/examples/tree-view.html

var new_tree = function(root_data) {
	return new Vue({
		el: '#demo',
		computed: {
			hasError: function () {
				return this.error != ''
			}
		},
		data: function() {
      return {
				error: '',
				treeData: root_data,
				selectedNode: Object,
				showInput: false,
				showArrival: true
			}
		},
		methods: {
			/*
			update_d3_label: function() {
				alert(JSON.stringify(this.selectedNode))
				console.log(this.selectedNode.name)
				update(this.selectedNode)
			},
			*/
			foo: function(node) {
				console.log(this.$window_portal)
				console.log(node)
			},
			add_child: function(parent_id, label, callback) {
				axios.post('/api/topoi', {
						api_topos: {
							parent_id: parent_id,
							label: label
						}
					})
					.then(function (response) {
						// handle new root success
						// new child finally!
						callback(response.data, null);
					})
					.catch(function (error) {
						// handle new root error
						console.log(error);
						callback(undefined, "<p>"+error.response.statusText+". See logs for details.</p>")
					})
			},
			delete_leaf: function(id, callback) {
				axios.delete('/api/topoi/'+id)
					.then(function (response) {
						// handle new root success
						// new child finally!
						callback(response.data, null);
					})
					.catch(function (error) {
						// handle new root error
						console.log(error);
						callback(undefined, "<p>"+error.response.statusText+". See logs for details.</p>")
					})
			},
			update_label: function(id, label, callback) {
				axios.put('/api/topoi/'+id, {
						api_topos: {
							label: label
						}
					})
					.then(function (response) {
						// handle new root success
						// new child finally!
						callback(response.data, null);
					})
					.catch(function (error) {
						// handle new root error
						console.log(error);
						callback(undefined, "<p>"+error.response.statusText+". See logs for details.</p>")
					})
			},
			show_message: function(msg) {
				this.$root.error = msg
				// https://stackoverflow.com/questions/762011/whats-the-difference-between-using-let-and-var-to-declare-a-variable-in-jav
				let self = this;
      	setTimeout(function(){
      		self.$root.error = '';
      	}, 2000);
			}
		}
	})
}

var is_leaf = function(node, tree) {
	return find_node_by_parent_id(node.id, tree) == undefined
}

var find_node_by_parent_id = function(id, tree) {
	return tree.find(function(el){return el.parent_id == id})
}

var recurse_db2js = function(id, tree) {
	let children = tree.filter(function(el){return el.parent_id == id})
	if(children.length==0){
		return []
	}else{
		return children.map(function(el){if(is_leaf(el, tree)){return {name: el.label, id: el.id}}else{return {name: el.label, id: el.id, children: recurse_db2js(el.id, tree)}}})
	}
}

var db2js = function(the_db_tree) {
	let root = find_node_by_parent_id(null, the_db_tree)
	return {name: root.label, id: root.id, children: recurse_db2js(root.id, the_db_tree)}
}

var init_tree = function() {

	// Make a request for the Tree
	var t = new_tree({})
	axios.get('/api/topoi')
		.then(function (response) {
			// handle get tree success
			// boot up the demo
			
			if(response.data.length == 0) {
				// brand new tree – make a root
				//
				t.add_child(null, 'radix', function(data, error){
					if(data == undefined){
						console.log(error);
						t.show_message(error.response.data)
						// TODO blink node and display error message
					}else{
						let the_tree = {name: 'radix', id: data.id, children: []}
						t.treeData = the_tree
						init_d3(the_tree, t) // d3
					}
				})
			} else {
				// convert db tree to js tree
				let the_tree = db2js(response.data)
				t.treeData = the_tree
				init_d3(the_tree, t) // d3
			}

		})
		.catch(function (error) {
			// handle get tree error
			console.log(error);
			t.show_message(error.response.data)
		})
		.then(function () {
			// always executed
		});

}

Vue.prototype.$window_portal = window;

init_tree()

