
import Vue from 'vue/dist/vue.esm'

// import LI_Node from './components/li_node'

/////
//
// models:
//
// CREATE TABLE "entities" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name_or_title" varchar, "part_id" integer, "type_id" integer);
// CREATE TABLE "types" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "typename" varchar, "supertype_id" integer);
//
// api:
//
// (1)         api_entities GET    /api/entities(.:format)                                                                  api/entities#index
// (2)                      POST   /api/entities(.:format)                                                                  api/entities#create
// (3)           api_entity GET    /api/entities/:id(.:format)                                                              api/entities#show
// (4)                      PATCH  /api/entities/:id(.:format)                                                              api/entities#update
// (4)                      PUT    /api/entities/:id(.:format)                                                              api/entities#update
// (5)                      DELETE /api/entities/:id(.:format)                                                              api/entities#destroy
//                api_types GET    /api/types(.:format)                                                                     api/types#index
//                          POST   /api/types(.:format)                                                                     api/types#create
//                 api_type GET    /api/types/:id(.:format)                                                                 api/types#show
//                          PATCH  /api/types/:id(.:format)                                                                 api/types#update
//                          PUT    /api/types/:id(.:format)                                                                 api/types#update
//                          DELETE /api/types/:id(.:format)                                                                 api/types#destroy

// https://vuejs.org/v2/examples/tree-view.html

var new_tree = function() {
	return new Vue({
		el: '#demo',
		computed: {
			hasError: function () {
				return this.error != ''
			}
		},
		data: function() {
      return {
				error: '',
				treeData: {},
				selectedNode: Object,
				showInput: false,
				showArrival: true
			}
		},
		methods: {
			// ignore this!
			portal: function(node) {
				console.log(this.$window_portal)
				console.log(node)
			},

			// (1)
			//
			// get all entities

			// (2)
			add_child: function(parent_id, label, callback) {
				axios.post('/api/entities', {
						api_topos: {
							part_id: parent_id,
							name_or_title: label
						}
					})
					.then(function (response) {
						// handle new root success
						// new child finally!
						callback(response.data, null);
					})
					.catch(function (error) {
						// handle new root error
						console.log(error);
						callback(undefined, "<p>"+error.response.statusText+". See logs for details.</p>")
					})
			},

			// (3)
			//
			// show leaf

			// (4)
			update_label: function(id, label, callback) {
				axios.put('/api/entities/'+id, {
						api_topos: {
							name_or_title: label
						}
					})
					.then(function (response) {
						// handle new root success
						// new child finally!
						callback(response.data, null);
					})
					.catch(function (error) {
						// handle new root error
						console.log(error);
						callback(undefined, "<p>"+error.response.statusText+". See logs for details.</p>")
					})
			},

			// (5)
			// can't just destroy any ol' node, ust be a leaf
			destroy_leaf: function(id, callback) {
				axios.delete('/api/entities/'+id)
					.then(function (response) {
						// handle new root success
						// new child finally!
						callback(response.data, null);
					})
					.catch(function (error) {
						// handle new root error
						console.log(error);
						callback(undefined, "<p>"+error.response.statusText+". See logs for details.</p>")
					})
			},

			flash_error: function(msg) {
				this.$root.error = msg
				// https://stackoverflow.com/questions/762011/whats-the-difference-between-using-let-and-var-to-declare-a-variable-in-jav
				let self = this;
      	setTimeout(function(){
      		self.$root.error = '';
      	}, 2000);
			}
		}
	})
}

var is_leaf = function(node, tree) {
	return find_node_by_parent_id(node.id, tree) == undefined
}

var find_node_by_parent_id = function(id, tree) {
	return tree.find(function(el){return el.parent_id == id})
}

var recurse_db2js = function(id, tree) {
	let children = tree.filter(function(el){return el.parent_id == id})
	if(children.length==0){
		return []
	}else{
		return children.map(function(el){if(is_leaf(el, tree)){return {name: el.label, id: el.id}}else{return {name: el.label, id: el.id, children: recurse_db2js(el.id, tree)}}})
	}
}

var db2js = function(the_db_tree) {
	let root = find_node_by_parent_id(null, the_db_tree)
	return {name: root.label, id: root.id, children: recurse_db2js(root.id, the_db_tree)}
}

var init_tree = function() {

	// Make a request for the Tree
	var t = new_tree()
	axios.get('/api/entities')
		.then(function (response) {
			// handle get tree success
			// boot up the demo
			
			// let the_tree = {};
			if(response.data.length == 0) {
				// brand new tree – make a root
				//
				/*
				t.add_child(null, 'radix', function(data, error){
					if(data == undefined){
						console.log(error);
						t.flash_error(error.response.data)
						// TODO blink node and display error message
					}else{
						the_tree = {name: 'radix', id: data.id, children: []}
						t.treeData = the_tree
						init_d3(the_tree, t) // d3
					}
				})
				*/
			} else {
				// convert db tree to js tree
				the_tree = db2js(response.data)
				t.treeData = the_tree
				// init_d3(the_tree, t) // d3
			}
			// init_d3(the_tree, t) // how i shoulda done it
		})
		.catch(function (error) {
			// handle get tree error
			console.log(error);
			t.flash_error(error.response.data)
		})
		.then(function () {
			// always executed
		});

}

Vue.prototype.$window_portal = window;

init_tree()

