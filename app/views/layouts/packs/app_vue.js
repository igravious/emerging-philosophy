
import Vue from 'vue/dist/vue.esm' // template thingy (dev mode)
import App from './components/App'

Vue.config.productionTip = false

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(VueMaterial)

const content = document.getElementById(window.app_root).innerHTML.slice(0)
//console.log(content)

require('stylesheets/the_app.css')

import TableOfRecords from './components/table-of-records'

import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)

import Loggy from './plugins/loggy-vue'
Vue.use(Loggy)

import RestApiMixin from './mixins/rest-api'

import hljs from 'highlight.js/lib/highlight';
import json from 'highlight.js/lib/languages/json';
hljs.registerLanguage('json', json);
import 'highlight.js/styles/github.css';
import Highlight from './components/highlight'
hljs.initHighlightingOnLoad();

new Vue({
	mixins: [RestApiMixin],
		data: function () {
			return {
				henry: this.$logger,
			}
		},
  el: '#'+window.app_root,
  components: {
		'App': App,
		'table-of-records': TableOfRecords,
		'highlight': Highlight
	},
	template: '<App>'+content+'</App>',
	methods: {
		refName(idx) {
			return 'T'+idx
		},
		apiTrigger: function(payload) {
			this[payload.fn](payload.slot, payload.callback, payload.data)
		},
		tableMounted: function(table) {
			table.notificationData.noticeLevel = 1
			table.getRecords()
		}
	}
})
