# class Work < ApplicationRecord
# 	self.primary_key = 'item'
# end

class Work < Wikidatum
	def self.only_genre
		self.where(q: GenreOf.pluck(:of_id).uniq)
	end
	has_many :genres_of, class_name: "GenreOf"
	has_many :genres, through: :genres_of
	def self.only_author
		self.where(q: AuthorOf.pluck(:of_id).uniq)
	end
	has_many :authors_of, class_name: "AuthorOf"
	has_many :authors, through: :authors_of
end
