class Genre < Wikidatum
	has_many :genres_of, class_name: "GenreOf"
	has_many :works, through: :genres_of
end
