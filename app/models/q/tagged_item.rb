class Q::TaggedItem < ApplicationRecord
  belongs_to :q_item, :class_name => 'Q::Item'
  belongs_to :q_tag, :class_name => 'Q::Tag'
  belongs_to :q_source, :class_name => 'Q::Source'
end
