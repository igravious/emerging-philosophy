class Q::Author < Q::Entity

	attribute :full_name, :string

	def has_attribute?(attr_name)
		if "full_name" == attr_name
			return true
		else
			super(attr_name)
		end
	end

	def read_attribute(attr_name)
		if "full_name" == attr_name
			full_name
		else
			super(attr_name)
		end
	end

	def full_name
		if self.name.nil? or self.name.empty?
			self.first_name + ' ' + self.last_name
		else
			self.name.dup
		end
	end

	def self.attribute_names
		Q::Entity.attribute_names+["full_name"]-["name", "first_name", "last_name"]
	end

	def attribute_names
		self.class.attribute_names
	end

	def attributes
		attrs = super.dup
		attrs["full_name"] = self.full_name
		attrs.delete("name")
		attrs.delete("first_name")
		attrs.delete("last_name")
		attrs
	end

	class << self
		# column_defaults

		# column_for_attribute

		def columns
			columns_hash.values
		end

		def columns_hash
			x = Q::Entity.columns_hash.dup
			sql_type_metadata = OpenStruct.new(type: :string)
			x["full_name"] = ActiveRecord::ConnectionAdapters::Column.new("full_name", '', sql_type_metadata , false, Q::Entity.table_name)
			x.delete("name")
			x.delete("first_name")
			x.delete("last_name")
			x
		end
	end

end

# "id"=>#<ActiveRecord::ConnectionAdapters::Column:0x00007f23588eef90 @name="id", @table_name="q_entities", @sql_type_metadata=#<ActiveRecord::ConnectionAdapters::SqlTypeMetadata:0x00007f23588eefe0 @sql_type="integer", @type=:integer, @limit=nil, @precision=nil, @scale=nil>, @null=false, @default=nil, @default_function=nil, @collation=nil, @comment=nil>

# "name"=>#<ActiveRecord::ConnectionAdapters::Column:0x000000000420a6a8 @name="name", @table_name="q_entities", @sql_type_metadata=#<ActiveRecord::ConnectionAdapters::SqlTypeMetadata:0x000000000420a798 @sql_type="varchar", @type=:string, @limit=nil, @precision=nil, @scale=nil>, @null=true, @default=nil, @default_function=nil, @collation=nil, @comment=nil>	
