class Q::Work < ApplicationRecord

	# Q::Work is an abstract class?
	
	def self._columns
		Q::Item.attribute_names - ['key', 'type']
	end

	self._columns.each do |attr_name|
		attribute attr_name.to_sym
	end

	def has_attribute?(attr_name)
		self._columns.include?(attr_name)
	end

	def read_attribute(attr_name)
		if self._columns.include?(attr_name)
			# hmm
			self.send(attr_name.to_sym)
		else
			super(attr_name)
		end
	end

	def self.attribute_names
		self._columns
	end

	def attribute_names
		self.class.attribute_names
	end

	# hmm
	def attributes
		attrs = []
		self._columns.each do |attr_name|
			attrs[attr_name] = self.send(attr_name.to_sym)
		end
	end

	class << self
		def columns
			columns_hash.values
		end

		def columns_hash
			Q::Item.columns_hash
		end
	end
end
