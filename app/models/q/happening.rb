class Q::Happening < ApplicationRecord
  belongs_to :q_item, :class_name => 'Q::Item'
  belongs_to :q_verb, :class_name => 'Q::Verb'
  belongs_to :q_source, :class_name => 'Q::Source'
end
