class Q::TaggedWork < ApplicationRecord
  belongs_to :q_item, :class_name => 'Q::Item'
  belongs_to :q_tag, :class_name => 'Q::Tag'
end
