class Q::CriticalWriting < Q::Work

#Q::Item.pluck(:type).uniq!
#(0.4ms)  SELECT "q_items"."type" FROM "q_items"
#["Q::JournalArticle", "Q::Webpage", "Q::BlogPost", "Q::Book", "Q::BookSection", "Q::Report", "Q::ConferencePaper", "Q::NewspaperArticle", "Q::ComputerProgram", "Q::EncyclopediaArticle"]
	def self.types
		["Q::JournalArticle", "Q::Webpage", "Q::BlogPost", "Q::Book", "Q::BookSection", "Q::Report", "Q::ConferencePaper", "Q::NewspaperArticle", "Q::EncyclopediaArticle"]
	end

end
