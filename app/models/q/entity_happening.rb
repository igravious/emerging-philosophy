class Q::EntityHappening < ApplicationRecord
  belongs_to :q_entity, :class_name => 'Q::Entity'
  belongs_to :q_happening, :class_name => 'Q::Happening'
end
