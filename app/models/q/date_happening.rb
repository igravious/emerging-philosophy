class Q::DateHappening < ApplicationRecord
  belongs_to :q_the_date, :class_name => 'Q::TheDate'
  belongs_to :q_happening, :class_name => 'Q::Happening'
end
