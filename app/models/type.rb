class Type < ApplicationRecord

	def self.link_name
		'supertype_id'
	end

	# don't give an association name the same name as an attribute, no good can come of it
	belongs_to :supertype, class_name: 'Type', optional: true # Top has no ancestor
	has_many   :subtypes,  class_name: 'Type', foreign_key: link_name

	# typing relation
	has_many   :instances, class_name: 'Entity', foreign_key: 'type_id'

	require 'recursive_sql'
	include RecursiveSQL::InstanceMethods
	extend RecursiveSQL::ClassMethods

end
