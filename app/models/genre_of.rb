class GenreOf < ApplicationRecord # naming is hard :(
 	self.primary_keys = :genre_id,:of_id

	belongs_to :genre, primary_key: :q #foreign_key: :genre_id
	# :of_id should be :work_id ?
	belongs_to :work,  primary_key: :q, foreign_key: :of_id
end
