class Entity < ApplicationRecord

	def self.link_name
		'part_id'
	end

	# recursive relationship association defintions
	belongs_to :part_of, class_name: 'Entity', foreign_key: link_name, optional: true
	has_many   :parts,   class_name: 'Entity', foreign_key: link_name

	# type relationship association definition
	belongs_to :type

	require 'recursive_sql'
	include RecursiveSQL::InstanceMethods
	extend RecursiveSQL::ClassMethods

end
