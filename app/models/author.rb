class Author < Wikidatum
	has_many :authors_of, class_name: "AuthorOf"
	has_many :works, through: :authors_of
end
