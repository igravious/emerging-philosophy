class Api::QEntitiesController < ApplicationController
	before_action :set_q_entity, only: [:show, :update, :destroy]

	# GET /q_entities
  def index
		@q_entities = Q::Entity.all

		render json: @q_entities
  end

	# GET /q_entities/1
  def show
		render json: @q_entity
  end

	# POST /q_entities
  def create
		@q_entity = Q::Entity.new(q_entity_params)

		if @q_entity.save
			render json: @q_entity, status: :created, location: "/api/q_entities/#{@q_entity.id}" # what's the best way to do this?
    else
			render json: @q_entity.errors, status: :unprocessable_entity
    end
  end

	# PATCH/PUT /q_entities/1
  def update
		if @q_entity.update(q_entity_params)
			render json: @q_entity
    else
			render json: @q_entity.errors, status: :unprocessable_entity
    end
  end

	# DELETE /q_entities/1
  def destroy
		@q_entity.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
	  def set_q_entity
			@q_entity = Q::Entity.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
	  def q_entity_params
			params.require(:q_entity).permit(:name, :first_name, :last_name)
    end
end
