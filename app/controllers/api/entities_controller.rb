class Api::EntitiesController < ApplicationController

	# GET /api/entities
  def index
		entities = Entities.all
		if entities.size == 0
			render json: []
		else
			render json: entities
		end
  end

end
