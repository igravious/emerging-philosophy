class Api::WorksController < ApplicationController

	# GET /api/works
  def index
		@works = Work.all
		if @works.size == 0
			render json: []
		else
			render json: @works
		end
  end

end
