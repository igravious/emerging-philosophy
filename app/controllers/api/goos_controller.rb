class Api::GoosController < ApplicationController
  before_action :set_goo, only: [:show, :update, :destroy]

  # GET /goos
  def index
    @goos = Goo.all

    render json: @goos
  end

  # GET /goos/1
  def show
    render json: @goo
  end

  # POST /goos
  def create
    @goo = Goo.new(goo_params)

    if @goo.save
      render json: @goo, status: :created, location: "/api/foos/#{@goo.id}" # what's the best way to do this?
    else
      render json: @goo.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /goos/1
  def update
    if @goo.update(goo_params)
      render json: @goo
    else
      render json: @goo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /goos/1
  def destroy
    @goo.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goo
      @goo = Goo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def goo_params
      params.require(:goo).permit(:wham, :whaz, :whar)
    end
end
