class Api::QCriticalWritingsController < ApplicationController
	before_action :set_q_critical_writing, only: [:show, :update, :destroy]

	# GET /q_critical_writings
  def index
		@q_critical_writings = Q::CriticalWriting.all

		render json: @q_critical_writings
  end

	# GET /q_critical_writings/1
  def show
		render json: @q_critical_writing
  end

	# POST /q_critical_writings
  def create
		@q_critical_writing = Q::CriticalWriting.new(q_critical_writing_params)

		if @q_critical_writing.save
			render json: @q_critical_writing, status: :created, location: "/api/q_critical_writings/#{@q_critical_writing.id}" # what's the best way to do this?
    else
			render json: @q_critical_writing.errors, status: :unprocessable_entity
    end
  end

	# PATCH/PUT /q_critical_writings/1
  def update
		if @q_critical_writing.update(q_critical_writing_params)
			render json: @q_critical_writing
    else
			render json: @q_critical_writing.errors, status: :unprocessable_entity
    end
  end

	# DELETE /q_critical_writings/1
  def destroy
		@q_critical_writing.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
	  def set_q_critical_writing
			@q_critical_writing = Q::CriticalWriting.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
	  def q_critical_writing_params
			params.require(:q_critical_writing).permit(:title, :description, :date, :short_title, :url, :rights, :extra)
    end
end
