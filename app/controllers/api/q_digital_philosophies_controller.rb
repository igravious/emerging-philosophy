class Api::QDigitalPhilosophiesController < ApplicationController
	before_action :set_q_digital_philosophy, only: [:show, :update, :destroy]

	# GET /q_digital_philosophies
  def index
		@q_digital_philosophies = Q::DigitalPhilosophy.all

		render json: @q_digital_philosophies
  end

	# GET /q_digital_philosophies/1
  def show
		render json: @q_digital_philosophy
  end

	# POST /q_digital_philosophies
  def create
		@q_digital_philosophy = Q::DigitalPhilosophy.new(q_digital_philosophy_params)

		if @q_digital_philosophy.save
			render json: @q_digital_philosophy, status: :created, location: "/api/q_digital_philosophies/#{@q_digital_philosophy.id}" # what's the best way to do this?
    else
			render json: @q_digital_philosophy.errors, status: :unprocessable_entity
    end
  end

	# PATCH/PUT /q_digital_philosophies/1
  def update
		if @q_digital_philosophy.update(q_digital_philosophy_params)
			render json: @q_digital_philosophy
    else
			render json: @q_digital_philosophy.errors, status: :unprocessable_entity
    end
  end

	# DELETE /q_digital_philosophies/1
  def destroy
		@q_digital_philosophy.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
	  def set_q_digital_philosophy
			@q_digital_philosophy = Q::DigitalPhilosophy.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
	  def q_digital_philosophy_params
			params.require(:q_digital_philosophy).permit(:title, :description, :date, :short_title, :url, :rights, :extra)
    end
end
