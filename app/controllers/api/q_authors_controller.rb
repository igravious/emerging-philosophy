class Api::QAuthorsController < ApplicationController
	before_action :set_q_author, only: [:show, :update, :destroy]

	# GET /q_authors
  def index
		@q_authors = Q::Author.all

		render json: @q_authors
  end

	# GET /q_authors/1
  def show
		render json: @q_author
  end

	# POST /q_authors
  def create
		@q_author = Q::Author.new(q_author_params)

		if @q_author.save
			render json: @q_author, status: :created, location: "/api/q_authors/#{@q_author.id}" # what's the best way to do this?
    else
			render json: @q_author.errors, status: :unprocessable_entity
    end
  end

	# PATCH/PUT /q_authors/1
  def update
		if @q_author.update(q_author_params)
			render json: @q_author
    else
			render json: @q_author.errors, status: :unprocessable_entity
    end
  end

	# DELETE /q_authors/1
  def destroy
		@q_author.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
	  def set_q_author
			@q_author = Q::Author.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
	  def q_author_params
			params.require(:q_author).permit(:full_name)
    end
end
