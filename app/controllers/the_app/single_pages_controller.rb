class TheApp::SinglePagesController < BaseController
  before_action :set_layout

	def index
	end

	# helper_method :record_attributes
	
	require 'record_helper'

	def record_attributes(model)
		table_name = mdl2tbl(model)
		record_name = model.to_s
		record_attr_names = model.attribute_names - [model.primary_key, "created_at", "updated_at"]
		record_attr_types = {}
		record_defaults = {}
		record_nulls = {}
		ruby_to_html5input = {
			:date => "date",
			:integer => "number",
			:string => "text"
		}
		# Rails.logger.info model.columns_hash
		record_attr_names.each do |v|
			# Rails.logger.warn "===> #{v.inspect}"
			record_attr_types[v] = ruby_to_html5input[model.columns_hash[v].type]
			record_defaults[v] = model.columns_hash[v].default
			record_nulls[v] = model.columns_hash[v].null
		end
		r = {}
		r[:record_name],r[:attr_names],r[:attr_types],r[:defaults],r[:nulls],r[:table_name]=[record_name,record_attr_names,record_attr_types,record_defaults,record_nulls,table_name]
		r
	end

	#

	def a_record(rec_const)
		@record_name, @record_attr_names, @record_attr_types, @record_defaults, @record_nulls, @table_name = record_attributes(rec_const).values
	end

	# dynamically generate these

	def foos
		a_record(Foo) # writeable defaults to true?!
		render :record
	end

	def goos
		a_record(Goo) # writeable defaults to true?!
		render :record
	end

	def foos_n_goos
		@mr = [] # many records
		@mr[0] = record_attributes(Foo)
		@mr[1] = record_attributes(Goo)
		render 'many_records'
	end

	# the playground
	
	def records
		a_record(Record)
		render :record
	end

	def playground
		@mr = [] # many records
		@mr[0] = {}
		@mr[0][:record_name], @mr[0][:attr_names], @mr[0][:attr_types], @mr[0][:defaults], @mr[0][:nulls], @mr[0][:table_name] = record_attributes(Record)
		render 'many_records'
	end

	# the kb (automate!)

	def q_authors
		respond_to do |format|
			format.html do
				a_record(Q::Author)
				@writeable = false
				render :record
			end
			format.json { redirect_to api_q_authors_url }
		end
	end

	def q_digital_philosophies
		respond_to do |format|
			format.html do
				a_record(Q::DigitalPhilosophy)
				@writeable = false
				render :record
			end
			format.json { redirect_to api_q_authors_url }
		end
	end

	def q_critical_writings
		respond_to do |format|
			format.html do
				a_record(Q::CriticalWriting)
				@writeable = false
				render :record
			end
			format.json { redirect_to api_q_authors_url }
		end
	end

	def knowledgebase
		@mr = []
		@mr[0] = record_attributes(Q::DigitalPhilosophy)
		@mr[1] = record_attributes(Q::CriticalWriting)
		@mr[2] = record_attributes(Q::Author)
		render 'many_records'
	end

	#

	def the_home
	end

	def about
	end

	INITIAL_SEGMENT_REGEX = %r{^\/([^\/\(:]+)}
	APP_ROOT = Rails.configuration.x.the_app.app_root
	APP_SEGMENT_REGEX = /^\/#{APP_ROOT}(.*)\(.+/

	# https://gist.github.com/bantic/5688232
	# “Programmatically list all routes (/paths) from within a rails app.”

	def match_initial_path_segment(path)
		if match = APP_SEGMENT_REGEX.match(path)
			Rails.logger.info match.inspect
			match[1]
		end
	end

	RAW_DATA_DIR = Rails.configuration.x.zotero.raw_data_dir
	WORKS = Rails.configuration.x.zotero.the_works

	def raw_data
		fl = Dir.glob(RAW_DATA_DIR+"/*"+WORKS)
		fn = fl.sort.last
		@raw_data = File.read(fn)
	end

	def routes
		routes = Rails.application.routes.routes
    paths = routes.collect {|r| r.path.spec.to_s }
		Rails.logger.info paths
		@initial_path_segments ||= begin
			paths.collect {|path|
				match_initial_path_segment(path)
			}.compact.uniq
		end
	end

	def index
		redirect_to self.send(APP_ROOT+'_routes_url')
	end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_layout
			if params['layout']
				@layout = params['layout']
			else
      	@layout = APP_ROOT
			end
    end

    # Only allow a trusted parameter "white list" through.
    def single_page_params
      params.require(:record).permit(:layout)
    end
end
