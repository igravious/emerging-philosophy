class Each::SinglePagesController < BaseController
  before_action :set_layout

	def index
	end

	# helper_method :record_attributes

	def record_attributes(table)
		record_name = table.to_s
		record_attr_names = table.attribute_names - [table.primary_key, "created_at", "updated_at"]
		record_attr_types = {}
		record_defaults = {}
		record_nulls = {}
		ruby_to_html5input = {
			:date => "date",
			:integer => "number",
			:string => "text"
		}
		record_attr_names.each do |v|
			record_attr_types[v] = ruby_to_html5input[table.columns_hash[v].type]
			record_defaults[v] = table.columns_hash[v].default
			record_nulls[v] = table.columns_hash[v].null
		end
		return [record_name, record_attr_names, record_attr_types, record_defaults, record_nulls]
	end

	def many_records
		@mr = [] # many records
		@mr[0] = {}
		@mr[0][:name], @mr[0][:attr_names], @mr[0][:attr_types], @mr[0][:defaults], @mr[0][:nulls] = record_attributes(Foo)
		@mr[1] = {}
		@mr[1][:name], @mr[1][:attr_names], @mr[1][:attr_types], @mr[1][:defaults], @mr[1][:nulls] = record_attributes(Goo)
	end

	def foos
		@record_name, @record_attr_names, @record_attr_types, @record_defaults, @record_nulls = record_attributes(Foo)
		# helpers.record_attributes
		render :record
	end

	def goos
		@record_name, @record_attr_names, @record_attr_types, @record_defaults, @record_nulls = record_attributes(Goo)
		# helpers.record_attributes
		render :record
	end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_layout
     	@layout = "each"
    end

    # Only allow a trusted parameter "white list" through.
    def single_page_params
      params.require(:record).permit(:layout)
    end
end
