require "rails_helper"

RSpec.describe Api::GoosController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/goos").to route_to("api/goos#index")
    end

    it "routes to #show" do
      expect(:get => "/api/goos/1").to route_to("api/goos#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/api/goos").to route_to("api/goos#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/api/goos/1").to route_to("api/goos#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/api/goos/1").to route_to("api/goos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/api/goos/1").to route_to("api/goos#destroy", :id => "1")
    end
  end
end
