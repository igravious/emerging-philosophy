require "rails_helper"

RSpec.describe TheApp::SinglePagesController, type: :routing do
  describe "routing" do
    it "routes to #many_records" do
      expect(:get => "/records").to route_to("single_pages#many_records")
    end

    it "routes to #goos" do
      expect(:get => "/record/foos").to route_to("single_pages#foos")
    end

    it "routes to #goos" do
      expect(:get => "/record/goos").to route_to("single_pages#goos")
    end
  end
end
