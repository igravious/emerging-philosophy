/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('foos', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    bar: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    baz: {
      type: DataTypes.STRING,
      allowNull: true
    },
    bam: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'foos'
  });
};
