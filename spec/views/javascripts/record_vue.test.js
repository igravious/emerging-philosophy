
import Vue from "vue";
// stops Vue whining
import { mount } from '@vue/test-utils'

//import App from "packs/records_vue.js"

import T from "packs/components/table-of-records.vue"

import VuePaginate from 'vue-paginate'
Vue.use(VuePaginate)

import L from "packs/plugins/loggy_vue.js"
Vue.use(L);

// mock stores :)

var date1 = new Date().toISOString()
var mockFoo = [
	{id: 100, bar: 666, baz: 'wiggle', bam: '2019-03-17', created_at: date1, updated_at: date1},
	{id: 200, bar: 999, baz: 'buggle', bam: '2019-01-01', created_at: date1, updated_at: date1}
]

var date2 = new Date().toISOString()
var mockGoo = []
/*
var mockGoo = [
	{id: 300, whar: 123, whaz: 'wiggle', wham: '2019-03-17', created_at: date2, updated_at: date2},
	{id: 400, whar: 456, whaz: 'buggle', wham: '2019-01-01', created_at: date2, updated_at: date2}
]
*/

// am not using Vue test tools and Vue router for the moment

var mockAPI = {
	// Rails REST fns
	index_foos: function(callback, filter) {
		callback(mockFoo, undefined) // no error
	},
	index_goos: function(callback, filter) {
		callback(mockGoo, undefined) // no error
	},
	create_foos: function(callback, rec) {
		rec['id'] = 1000 // gen new unique id!
		var date = new Date().toISOString()
		rec['created_at'] = date
		rec['updated_at'] = date
		mockFoo.push(rec) // do i need to go into this detail?
		callback(rec, undefined) // no error
	},
	create_goos: function(callback, rec) {
		var new_rec = {}
		new_rec['id'] = 1000 // gen new unique id!
		var date = new Date().toISOString()
		new_rec['created_at'] = date
		new_rec['updated_at'] = date
		for (let name in this.columnNames) {
			new_rec[name] = rec[name]
		}
		mockGoo.push(new_rec) // do i need to go into this detail?
		callback(new_rec, undefined) // no error
	},
	update: function(callback, rec) {
		throw 'not implemented'
	},
	destroy: function(callback, id) {
		// remove row with given id from mockFoo
		callback(null, undefined) // no error
	}
}

const db_filename = 'db/test.sqlite3'
var fs = require('fs');

const noisy = false

function test_logger(msg) {
	if (noisy) {
		console.log(msg)
	}
}

const Sequelize = require('sequelize');
const sequelize = new Sequelize({
	dialect: 'sqlite',
	storage: db_filename,
	logging: test_logger,
	define: {
		timestamps: false
	}
});

// node node_modules/.bin/sequelize-auto -e sqlite -h localhost -d db/test.sqlite3 -o spec/views/javascripts/ -t foos,goos

const models = [
	{model_name: 'Foo', model: sequelize.import(__dirname + "/foos.js")},
	{model_name: 'Goo', model: sequelize.import(__dirname + "/goos.js")}
]

debugger

/*
 * app/controllers/single_pages_controller.rb
 */

function removeElementFromArrayByValue(arr, val) {
	var index = arr.indexOf(val)
	if (index > -1) {
		arr.splice(index, 1)
	}
}

const js_to_html5input = {
	'DATE': 'date',					// DATEONLY
	'INTEGER': 'number',		// INTEGER
	'VARCHAR(255)': 'text'	// STRING
}

function modelToProps(model_name, model) {
	let props = {}
	props['defaultAmountPerPage'] = 5
	props['recordName'] = model_name // ?
	let attrs = model.tableAttributes
	let keys = Object.keys(attrs).splice(0)
	removeElementFromArrayByValue(keys, 'id')
	removeElementFromArrayByValue(keys, 'created_at')
	removeElementFromArrayByValue(keys, 'updated_at')
	props['columnNames'] = keys
	props['columnTypes'] = {}
	props['columnDefaults'] = {}
	props['columnNulls'] = {}
	keys.forEach((key) => {
		props['columnTypes'][key] = js_to_html5input[attrs[key].type]
		props['columnDefaults'][key] = attrs[key].defaultValue
		props['columnNulls'][key] = attrs[key].allowNull // if it's true it allows null
	})
	return props
}

const E = Vue.extend(T)
var wrapper = null

function api(payload) {
	return payload.fn+'_'+payload.slot.split('/')[2]
}

describe('Table of Records', () => {

	/*
	 * how to order these?
	 */
	test("the DB `"+db_filename+"' exists", () => {
		expect(fs.existsSync(db_filename)).toBe(true)
	})

	test('the DB authenticates ', () => {
		return expect(sequelize.authenticate()).resolves.toBe(undefined); // it merely matters that it resolves tbh
	});

	/*
	 * ugh
	 */
	/*
	beforeEach(() => {
		// Now mount the component and you have the wrapper
		wrapper = mount(new E({
			propsData: modelToProps(models[0].model_name, models[0].model) 
		}))
		
		// You can access the actual Vue instance via `wrapper.vm` (if you want)
		const vm = wrapper.vm
		
		// To inspect the wrapper deeper just log it to the console
		// and your adventure with the Vue Test Utils begins
		console.log(wrapper)
	});

	afterEach(() => { 
	});
	*/

	const C = new E({
		propsData: modelToProps(models[0].model_name, models[0].model) 
	}).$mount()
	C.$logger.logToConsole = noisy
	C.notificationData.noticeLevel = 1

	const D = new E({
		propsData: modelToProps(models[1].model_name, models[1].model) 
	}).$mount()
	C.$logger.logToConsole = noisy
	C.notificationData.noticeLevel = 1

	/*
	it('test?', () => {
		expect(C.env).toBe('test')
	});
	*/

	test('A', () => {
		C.notificationMethods('danger',  'DA')
		C.notificationMethods('warning', 'WA')
		C.notificationMethods('info',    'IN')
		C.notificationMethods('success', 'SU')
		C.notificationMethods('debug',   'DE')
	})

	test('the #getRecord() chains component', function(done){
		expect(C.getRecords()).toEqual(C)
		done() // don't worry about async stuff
	});

	it('should have the correct slot', function(done){
		C.$on( 'api-trigger', function(payload) {
			// this is the Component
			expect(payload.slot).toEqual('/api/foos')
			done() // how do I do this?
		})
		var ret = C.getRecords() // returns Component so you can chain 
	});

	it('should also have the correct slot', function(done){
		D.$on( 'api-trigger', function(payload) {
			// this is the Component
			expect(payload.slot).toEqual('/api/goos')
			done() // how do I do this?
		})
		var ret = D.getRecords() // returns Component so you can chain 
	});

	it('should get two mocked records', function(done){
		C.$on( 'api-trigger', function(payload) {
			// this is the Component
			// console.warn(payload)
			/* { fn: 'index',
			 *   slot: '/api/foos',
			 *   callback: [Function: indexCallback] }
			 */
			expect(payload.fn).toEqual('index')
			mockAPI[api(payload)](payload.callback, payload.data)
			expect(this.numRecords).toEqual(2);
			done()
		})
		var ret = C.getRecords() // returns Component so you can chain 
	});

	it('should make a new record', () => {
		var num = D.numRecords // this needs to be a fn
		var idx = D.newRecord()-1
		expect(D.numRecords).toEqual(num+1)
	})

	it('should delete a record', () => { // these are all async unless i tear 'em all down in between!
		var num = D.numRecords
		var idx = D.newRecord()-1
		var ret = D.deleteRecord(idx)
		expect(D.numRecords).toEqual(num)
	})

	it('should delete a record', function(done) { // these are all async unless i tear 'em all down in between!
		D.$on( 'api-trigger', function(payload) {
			expect(payload.fn).toEqual('create')
			mockAPI[api(payload)](payload.callback, payload.data)
			expect(this.records[idx].attrs.id).toBeDefined()
			done()
		})
		var idx = D.newRecord()-1 // should it return the record or the array index?
		expect(D.records[idx].id).toBeUndefined()
		var ret = D.saveRecord(idx)
	})

	it('finds that one is equal to one', () => {
		expect(1).toEqual(1);
	});
});
