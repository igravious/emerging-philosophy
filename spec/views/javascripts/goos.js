/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('goos', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    wham: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    whaz: {
      type: DataTypes.STRING,
      allowNull: true
    },
    whar: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'goos'
  });
};
