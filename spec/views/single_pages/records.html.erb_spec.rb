require 'rails_helper'

RSpec.describe "single_pages/many_records", type: :view do

	def record_names
		@record_names = Foo.attribute_names - ["id", "created_at", "updated_at"]
	end

	def record_types
		@record_types = []
		ruby_to_html5input = {
			:date => "date",
			:integer => "number",
			:string => "text"
		}
		record_names.each_with_index do |v,i|
			@record_types[i] = ruby_to_html5input[Foo.columns_hash[v].type]
		end
	end

  before(:each) do
		assign(:record, 'foos')
		assign(:record_names, record_names)
		assign(:record_types, record_types)
    assign(:foos, [
      Foo.create!(
        :baz => "Igloo",
        :bar => 2
      ),
      Foo.create!(
        :baz => "Pipe",
        :bar => 3
      )
    ])
  end

  it "renders a list of foos" do
    render
		binding.pry
    assert_select "tr>td", :text => "Whaz".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
