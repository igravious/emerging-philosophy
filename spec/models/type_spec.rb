require 'rails_helper'

RSpec.describe Type, type: :model do
  pending "add complete examples to #{__FILE__} (and only then delete)"

	context 'Basic operations' do  # (almost) plain English
		it 'must be nameable' do
			n = 'Ex Nihilo'
			entity = Type.create!(:typename => n)
			expect(entity.typename).to eq(n)
		end

		it 'must be composable [1]' do   #
			parent = Type.create!
			child_one   = Type.create!(:supertype_id => parent.id)
			child_two   = Type.create!(:supertype_id => parent.id)
			expect(parent.subtypes.length).to eq(2)
		end

		it 'must be composable [2.a]' do   #
			parent = Type.create!
			child_one   = Type.create!(:supertype_id => parent.id)
			child_two   = Type.create!(:supertype_id => parent.id)
			expect(child_two.supertype).to eq(parent)
		end

		it 'must be composable [2.b]' do   #
			parent = Type.create!
			child_one   = Type.create!(:supertype_id => parent.id)
			child_two   = Type.create!(:supertype_id => parent.id)
			expect(child_one.supertype).to eq(parent)
		end
	end
end
