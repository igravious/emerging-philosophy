require 'rails_helper'

RSpec.describe Entity, type: :model do
  pending "add complete examples to #{__FILE__} (and only then delete)"

	mock = Type.create! # every entity _must_ have a type

	context 'when in doubt' do  # (almost) plain English
		it 'must be nameable' do
			n = 'Ex Nihilo'
			entity = Entity.create!(:name_or_title => n, :type_id => mock.id)
			expect(entity.name_or_title).to eq(n)
		end

		it 'must have parts' do   #
			parent = Entity.create!(:type_id => mock.id)
			child_one = Entity.create!(:part_id => parent.id, :type_id => mock.id)
			child_two = Entity.create!(:part_id => parent.id, :type_id => mock.id)
			expect(parent.parts.length).to eq(2)
		end

		it 'must be part of' do   #
			parent = Entity.create!(:type_id => mock.id)
			child_one = Entity.create!(:part_id => parent.id, :type_id => mock.id)
			child_two = Entity.create!(:part_id => parent.id, :type_id => mock.id)
			expect(child_one.part_of).to eq(parent)
			expect(child_two.part_of).to eq(parent)
		end

		it 'must be typeable' do
			t = Type.create!
			e = Entity.create!(:type_id => t.id)
			expect(e.type).to eq(t)
		end

		it 'must have instances' do
			t = Type.create!
			e1 = Entity.create!(:type_id => t.id)
			e2 = Entity.create!(:type_id => t.id)
			expect(t.instances.length).to eq(2)
		end

		it 'must act recursively below getting attrs' do
			top_level = Entity.create!(:type_id => mock.id)
			level_one = Entity.create!(:part_id => top_level.id, :type_id => mock.id)
			level_two = Entity.create!(:part_id => level_one.id, :type_id => mock.id)
			expect(top_level.tree_with_attrs.length).to eq(2)
		end

		it 'must act recursively below getting ids' do
			top_level = Entity.create!(:type_id => mock.id)
			level_one = Entity.create!(:part_id => top_level.id, :type_id => mock.id)
			level_two = Entity.create!(:part_id => level_one.id, :type_id => mock.id)
			expect(top_level.tree_with_ids.length).to eq(2)
		end

		it 'must act recursively below getting levels' do
			top_level = Entity.create!(:type_id => mock.id)
			level_one = Entity.create!(:part_id => top_level.id, :type_id => mock.id)
			level_two = Entity.create!(:part_id => level_one.id, :type_id => mock.id)
			expect(top_level.tree_with_levels.length).to eq(2)
		end

		it 'must act recursively getting attrs (inc. self)' do
			top_level = Entity.create!(:type_id => mock.id)
			level_one = Entity.create!(:part_id => top_level.id, :type_id => mock.id)
			level_two = Entity.create!(:part_id => level_one.id, :type_id => mock.id)
			expect(top_level.all_with_attrs.length).to eq(3)
		end

		it 'must act recursively getting ids (inc. self)' do
			top_level = Entity.create!(:type_id => mock.id)
			level_one = Entity.create!(:part_id => top_level.id, :type_id => mock.id)
			level_two = Entity.create!(:part_id => level_one.id, :type_id => mock.id)
			expect(top_level.all_with_ids.length).to eq(3)
		end

		it 'must act recursively getting levels (inc. self)' do
			top_level = Entity.create!(:type_id => mock.id)
			level_one = Entity.create!(:part_id => top_level.id, :type_id => mock.id)
			level_two = Entity.create!(:part_id => level_one.id, :type_id => mock.id)
			expect(top_level.all_with_levels.length).to eq(3)
		end

		# how to handle multiple roots ?
		# (spec and test)
		#
		# how to handle cycles ?
		# (spec and test)

		# test for destroying non-leaf
		#
		# test for destroying leaf
		
		# test for updating info

	end
end
