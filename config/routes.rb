Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get 'hello', to: "examples#hello" 

	# If you want to route /articles (without the prefix /admin) to Admin::ArticlesController, you could use:
	# scope module: 'admin' do
	#	  resources :articles, :comments
	# end

	# If you want to route /admin/articles to ArticlesController (without the Admin:: module prefix), you could use:
	# scope '/admin' do
	#	  resources :articles, :comments
	# end

	# If you want both!
	# namespace :admin do
	#	  resources :articles, :comments
	# end
	
	@told = false

	def single_pages(n)
		# The namespace's index page
		get n, to: n.to_s+'/single_pages#index', as: n.to_s+'_index'
		namespace n do
			# Record routes to SinglePages controller
			# resources :records
			if ActiveRecord::Base.connection.data_source_exists? 'records'

				# Page for playing around with records
				get 'playground', to: 'single_pages#playground'
				recs = Record.where(set: 'Playground')
				recs.each do |rec|
					action = rec.table_name
					scope '/playground' do
						get action, to: "single_pages##{action}", as: "playground_#{action}"
					end
				end

				# The knowledge base 
				get 'knowledgebase', to: 'single_pages#knowledgebase'
				recs = Record.where(set: 'Funfair')
				recs.each do |rec|
					action = rec.table_name
					scope '/knowledgebase' do
						get action, to: "single_pages##{action}", as: "kb_#{action}"
					end
				end

			else
				if not @told
					# STDERR.puts "ENV['RAILS_ENV']: #{ENV['RAILS_ENV']} – You may need to add `records'"
					STDERR.puts "Rails.env: #{Rails.env} – You may need to add `records'"
					@told = true
				end
			end

			# The home page
			get 'home', to: 'single_pages#the_home'
			# The catalogue page
			get 'catalogue', to: 'single_pages#catalogue'
			# The about page
			get 'about', to: 'single_pages#about'
			# The contact page
			get 'contact', to: 'single_pages#contact'
			# The settings page
			get 'settings', to: 'single_pages#settings'
			# The routes page
			get 'routes', to: 'single_pages#routes'
			# The timeline page
			get 'timeline', to: 'single_pages#timeline'
			# The raw data page
			get 'raw_data', to: 'single_pages#raw_data'
			# The publications page
			get 'publications', to: 'single_pages#publications'
			resources :single_pages, only: [] do
				collection do
					# get 'publications'
				end
			end
		end
	end

	single_pages(:the_app)

	single_pages(:each)

	# Namespace and version the api

	namespace :api do
		# zotero models (or windows onto them)
		resources :q_authors
		resources :q_digital_philosophies
		resources :q_critical_writings
		resources :records

		# super duper abstract model (?)
		resources :entities
		resources :works
		resources :types

		# tables for experimenting with
  	resources :foos
  	resources :goos
	end

end
