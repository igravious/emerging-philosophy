module Epsilon
  class Application < Rails::Application
		# https://guides.rubyonrails.org/configuring.html#custom-configuration
		config.x.the_app.app_root = 'the_app'
		
		config.x.zotero.raw_data_dir = File.join('tmp', 'raw_data')
		config.x.zotero.key_file = File.join('config', 'zotero.key')
		config.x.zotero.item_type = :item
		config.x.zotero.prefix = 'Q' # surprise, surprise :/
		config.x.zotero.suffix = 'X'
		config.x.zotero.collection = 'emerging philosophy'
		config.x.zotero.id = 1248965
		config.x.zotero.the_works = '_zotero_works.txt'
		config.x.zotero.the_attachments = '_zotero_attachments.txt'
		config.x.zotero.the_entries = '_zotero_entries.txt'

		config.x.canon.wikidata = 'https://query.wikidata.org/sparql'
		config.x.canon.dbpedia = 'http://dbpedia.org/sparql'
  end
end
