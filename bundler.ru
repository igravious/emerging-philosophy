require 'rubygems'
require 'bundler/setup'
require 'rack'

run( lambda { |env| [200, {}, ["Hello, Bundler!"]]} )
